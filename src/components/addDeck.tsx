import Box from "@mui/material/Box";
import { Hidden, Typography } from "@mui/material";
import LinearProgress from "@mui/material/LinearProgress";
import { useState } from "react";
import Link from "next/link";
import useFont from "@/hooks/fonts/useFont";
const { IBM_Plex_Sans_Thai } = useFont();
const imbPlexSansThai = IBM_Plex_Sans_Thai

interface AddDeckProps {
  deckTitle: string;
  deckMeaning: string;
  progress: number;
}

function AddDeck(props: AddDeckProps) {
  const [isHovered, setIsHovered] = useState(false);
  console.log(isHovered);
  return (
    <div>
      <Link href={'/createNewDeck'}>
        <Box
          sx={[
            // when hover
            {
              "&:hover": {
                // width: "183.39px",
                // height: "225.11px",
                border: 5,
                borderColor: "#22A699",
                boxShadow: "0px 15px #007469",
              },
            },
            {
              width: "173.39px",
              height: "225.11px",
              borderRadius: "25px",
              bgcolor: "#ffffff",
              boxShadow: "0px 15px #C8C8C8",
              padding: "10px",
              position: "relative",
            },
          ]}
          onMouseEnter={() => setIsHovered(true)}
          onMouseLeave={() => setIsHovered(false)}
        >
          {/* title and description*/}
          <Box
            sx={{
              display: "flex",
              flexDirection: "column",
              justifyContent: "center",
            }}
          >
            <Typography
              variant="h1"
              style={{
                fontFamily: imbPlexSansThai.style.fontFamily,
                fontWeight: 700,
                fontSize: "64px",
                color: "#303030",
                textAlign: "center",
              }}
            >
              <svg
                width="83"
                height="84"
                viewBox="0 0 83 84"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  d="M38.75 45.8506H18V38.934H38.75V18.184H45.6667V38.934H66.4167V45.8506H45.6667V66.6006H38.75V45.8506Z"
                  fill="black"
                />
              </svg>
            </Typography>
            <Typography
              variant="h1"
              style={{
                fontFamily: imbPlexSansThai.style.fontFamily,
                fontWeight: 400,
                fontSize: "24px",
                color: "#303030",
                textAlign: "center",
                padding: " 0px 20px",
              }}
            >
              Create new Deck
            </Typography>
          </Box>
        </Box>
      </Link>
      
    </div>
  );
}

export default AddDeck;
