import Box from "@mui/material/Box";
import { Hidden, Typography } from "@mui/material";
import LinearProgress from "@mui/material/LinearProgress";
import { useState } from "react";
import Link from "next/link";
import useFont from "@/hooks/fonts/useFont";
import { count } from "console";
const { IBM_Plex_Sans_Thai,IBM_Plex_Sans_KR } = useFont();

interface AddCardProps {
    index:Number
    word:String;
    pronunciationLanguage:String;
    pronunciationEng:String;
    pronunciationThai:String;
    translation:String;
    handleDelete:Function;
}

function AddCard(props: AddCardProps) {
    const index = props.index
    const word = props.word
    const pronunciationLanguage = props.pronunciationLanguage
    const pronunciationEng = props.pronunciationEng
    const pronunciationThai = props.pronunciationThai
    const translation = props.translation
    const handleDelete = props.handleDelete

    return(
    <Box
        sx={{
        marginTop: "20px",
        marginLeft: "0px",
        width: "1797px",
        height: "86px",
        display: "flex",
        flexDirection: "row",
        justifyContent: "space-between",
        }}
    >
        <Box
        sx={{
            width: "87px",
            height: "77px",
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
            background: "#22A699",
            borderRadius: "12px",
        }}
        >
        <Typography
            variant="h1"
            style={{
            fontFamily: IBM_Plex_Sans_Thai.style.fontFamily,
            fontWeight: 700,
            fontSize: "40px",
            color: "#ffffff",
            }}
        >
            {String(index)}
        </Typography>
        </Box>

        <Box
        sx={{
            width: "350px",
            height: "86px",
            border: "1px solid #303030",
            borderRadius: "20px",
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
        }}
        >
        <Typography
            variant="h1"
            style={{
            fontFamily: IBM_Plex_Sans_KR.style.fontFamily,
            fontWeight: 700,
            fontSize: "32px",
            color: "#303030",
            }}
        >
            {word}
        </Typography>
        </Box>

        <Box
        sx={{
            width: "265px",
            height: "86px",
            border: "1px solid #303030",
            borderRadius: "20px",
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
        }}
        >
        <Typography
            variant="h1"
            style={{
            fontFamily: IBM_Plex_Sans_KR.style.fontFamily,
            fontWeight: 400,
            fontSize: "32px",
            color: "#303030",
            }}
        >
            {pronunciationLanguage}
        </Typography>
        </Box>

        <Box
        sx={{
            width: "265px",
            height: "86px",
            border: "1px solid #303030",
            borderRadius: "20px",
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
        }}
        >
        <Typography
            variant="h1"
            style={{
            fontFamily: IBM_Plex_Sans_KR.style.fontFamily,
            fontWeight: 400,
            fontSize: "32px",
            color: "#303030",
            }}
        >
            {pronunciationEng}
        </Typography>
        </Box>

        <Box
        sx={{
            width: "265px",
            height: "86px",
            border: "1px solid #303030",
            borderRadius: "20px",
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
        }}
        >
        <Typography
            variant="h1"
            style={{
            fontFamily: IBM_Plex_Sans_KR.style.fontFamily,
            fontWeight: 400,
            fontSize: "32px",
            color: "#303030",
            }}
        >
            {pronunciationThai}
        </Typography>
        </Box>

        <Box
        sx={{
            width: "350px",
            height: "86px",
            border: "1px solid #303030",
            borderRadius: "20px",
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
        }}
        >
        <Typography
            variant="h1"
            style={{
            fontFamily: IBM_Plex_Sans_KR.style.fontFamily,
            fontWeight: 400,
            fontSize: "32px",
            color: "#303030",
            }}
        >
            {translation}
        </Typography>
        </Box>

        <Box 
        sx={{
            width: "100px",
            height: "86px",
            borderRadius: "20px",
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
        }}
        >
            <img style={{width:"75px"}}
            src="https://cdn.discordapp.com/attachments/1158294285343543336/1162474880348979210/image-removebg-preview.png?ex=653c1219&is=65299d19&hm=9400597ff5faf38b8d2ed816759a0630a2dcc782a2e57ab4cf10c45d4b9ce7c1&"
            alt="red"
            onClick={event=>handleDelete(Number(index)-1)}
            />
            {/* <button onClick={handleDelete({id:index,word,pronunciationLanguage,pronunciationEng,pronunciationThai,translation})}>test</button> */}
        </Box>
    </Box>
)}

export default AddCard;