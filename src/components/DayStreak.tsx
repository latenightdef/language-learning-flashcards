import { Box } from "@mui/material";
import Typography from "@mui/material/Typography";
import useFont from "@/hooks/fonts/useFont";

const { IBM_Plex_Sans_Thai } = useFont()



const imbPlexSansThai = IBM_Plex_Sans_Thai

interface DayStreak {
  streak: number;
}

export default function DayStreak(props: DayStreak) {
  return (
      <Box
      sx={{
        backgroundColor: "#ffffff",
        display: "flex",
        width: "277px",
        height: "98px",
        alignItems:"center",
        borderRadius:"30px",
        padding:"30px",
        gap:"30px",
        boxShadow:"0px 8px #C8C8C8"
      }}
    >
      <svg
        width="50"
        height="57"
        viewBox="0 0 50 57"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
      >
        <path
          d="M17.779 0.601054C18.6496 -0.211641 20 -0.200509 20.8705 0.612187C23.9509 3.49558 26.8415 6.60164 29.5424 9.96375C30.7701 8.36062 32.1652 6.61277 33.6719 5.18777C34.5536 4.36394 35.9152 4.36394 36.7969 5.19891C40.6585 8.87273 43.9286 13.7266 46.2277 18.3356C48.4933 22.8778 50 27.5202 50 30.7932C50 44.9987 38.8616 56.9999 25 56.9999C10.9821 56.9999 0 44.9876 0 30.7821C0 26.5071 1.98661 21.2858 5.06696 16.1202C8.1808 10.8766 12.5781 5.41043 17.779 0.601054ZM25.1897 46.3124C28.0134 46.3124 30.5134 45.5331 32.8683 43.9745C37.567 40.7014 38.8281 34.1554 36.0045 29.012C35.5022 28.01 34.2188 27.9432 33.4933 28.7893L30.6808 32.0513C29.9442 32.8973 28.6161 32.8751 27.9241 31.9956C26.0826 29.6577 22.7902 25.4829 20.9152 23.1116C20.2121 22.221 18.8728 22.2098 18.1585 23.1005C14.3862 27.8319 12.4888 30.8155 12.4888 34.1665C12.5 41.7925 18.1473 46.3124 25.1897 46.3124Z"
          fill="#F24C3D"
        />
      </svg>
      <Box sx={{ color: "#6F6F6F" }}>
        <Typography
          variant="h1"
          style={{
            fontFamily: imbPlexSansThai.style.fontFamily,
            fontWeight: 700,
            fontSize: "36px",
            color: "#303030",
          }}
        >
          {props.streak}
        </Typography>

        <Typography
          variant="h1"
          style={{
            fontFamily: imbPlexSansThai.style.fontFamily,
            fontWeight: 400,
            fontSize: "24px",
            color: "#C8C8C8",
          }}
        >
          Day streak
        </Typography>
      </Box>
    </Box>
  );
}
