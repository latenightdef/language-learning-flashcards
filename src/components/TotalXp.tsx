import { Box } from "@mui/material";
import Typography from "@mui/material/Typography";
import useFont from "@/hooks/fonts/useFont";

interface TotalXpProps {
  totalXp: number;
}

const { IBM_Plex_Sans_Thai } = useFont()
const imbPlexSansThai = IBM_Plex_Sans_Thai

export default function TotalXp(props: TotalXpProps) {
  return (
    <Box
      sx={{
        backgroundColor: "#ffffff",
        display: "flex",
        width: "301px",
        height: "98px",
        alignItems: "center",
        borderRadius: "30px",
        padding: "30px",
        gap: "30px",
        boxShadow: "0px 8px #C8C8C8",
      }}
    >
      <svg
        width="43"
        height="57"
        viewBox="0 0 43 57"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
      >
        <g clipPath="url(#clip0_134_449)">
          <path
            d="M0 28.5L3.15707 3.11719C3.37861 1.33594 4.88514 0 6.67969 0H25.3562C27.0178 0 28.3582 1.34707 28.3582 3.01699C28.3582 3.37324 28.2917 3.74062 28.1699 4.07461L23.041 17.8125H38.4719C40.7095 17.8125 42.5373 19.6383 42.5373 21.8982C42.5373 22.7221 42.2936 23.5236 41.8284 24.2027L20.5375 55.4859C19.884 56.4434 18.8095 57.0111 17.6685 57.0111H17.3472C15.6081 57.0111 14.1902 55.5861 14.1902 53.8383C14.1902 53.5822 14.2234 53.3262 14.2899 53.0701L19.4963 32.0625H3.54478C1.58407 32.0625 0 30.4705 0 28.5Z"
            fill="#F29727"
          />
        </g>
        <defs>
          <clipPath id="clip0_134_449">
            <rect width="42.5373" height="57" fill="white" />
          </clipPath>
        </defs>
      </svg>
      <div className="bg-white">
        <Typography
          variant="h1"
          style={{
            fontFamily: imbPlexSansThai.style.fontFamily,
            fontWeight: 700,
            fontSize: "36px",
            color: "#303030",
          }}
        >
          {props.totalXp}
        </Typography>

        <Typography
          variant="h1"
          style={{
            fontFamily: imbPlexSansThai.style.fontFamily,
            fontWeight: 400,
            fontSize: "24px",
            color: "#C8C8C8",
          }}
        >
          Total Xp
        </Typography>
      </div>
    </Box>
  );
}
