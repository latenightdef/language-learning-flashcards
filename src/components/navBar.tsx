import DayStreak from "./DayStreak";
import TotalXp from "./TotalXp";
import useAuth from "@/hooks/useAuth";
import Link from "next/link";
import { useEffect } from "react";
import getUserInfo from "@/pages/api/getUserInfo";

import * as React from "react";
import AppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";


import profile1 from "../assets/Profiles/profile_1.png";
import profile2 from "../assets/Profiles/profile_2.png";
import profile3 from "../assets/Profiles/profile_3.png";
import profile4 from "../assets/Profiles/profile_4.png";
import profile5 from "../assets/Profiles/profile_5.png";
import profile6 from "../assets/Profiles/profile_6.png";
import profile7 from "../assets/Profiles/profile_7.png";

import useFont from "@/hooks/fonts/useFont";

import { useRouter } from "next/router";

const { IBM_Plex_Sans_Thai, M_PLUS_C1 } = useFont()

const imbPlexSansThai = IBM_Plex_Sans_Thai

const titleList: { [key: string]: string } = {
  "/leagueBoard": "League board",
  "/profile": "Profile",
  "/decks": "Decks",
};

export default function Navbar() {
  const { setAuth,auth } = useAuth()
  const rank = auth.rank;
  const username =  auth.username
  const loginStreak = auth.loginStreak
  const totalXp = auth.totalXp
  const router = useRouter();
  const currentPath = router.asPath;

  useEffect(()=>{
    getUserInfo().then(
      (res) => {
        if(res.status === 200){
          setAuth(res.data)
          console.log(res.data)
        }
      }   
    )
  },[])

  const Profile =()=>{
    if(auth.profileImage=="profile1"){
        return profile1
    }else if(auth.profileImage=="profile2"){
        return profile2
    }else if(auth.profileImage=="profile3"){
        return profile3
    }else if(auth.profileImage=="profile4"){
        return profile4
    }else if(auth.profileImage=="profile5"){
        return profile5
    }else if(auth.profileImage=="profile6"){
        return profile6
    }else{
        return profile7
    }
}

  return (
    <AppBar
      position="static"
      sx={{
        backgroundColor: "#22A699",
        borderRadius: "0px 0px  50px 50px",
        padding: "30px 60px 30px 60px",
        boxShadow: 0,
      }}
    >
      <Box sx={{ display: "flex", justifyContent: "space-between" }}>
        {currentPath === "/home" ? (
          //Home Page
          <Box sx={{ display: "flex", gap: "40px", alignItems: "center" }}>
            <Typography
              variant="h1"
              style={{
                fontFamily: M_PLUS_C1.style.fontFamily,
                fontWeight: 800,
                fontSize: "64px",
                color: "#ffffff",
                padding: "0px 30px 0px 30px",
              }}
            >
              Synapz
            </Typography>
            <Box sx={{ display: "flex", gap: "20px", alignItems: "center" }}>
              <Link href={"/profile"}>
                <img
                  width="100px"
                  height="100px"
                  src={Profile().src}
                  alt="profileImage"
                />
              </Link>
              
              <Box
                sx={{
                  display: "flex",
                  flexDirection: "column",
                }}
              >
                <Typography
                  variant="h1"
                  style={{
                    fontFamily: imbPlexSansThai.style.fontFamily,
                    fontWeight: 700,
                    fontSize: "36px",
                    color: "#ffffff",
                  }}
                >
                  {username}
                </Typography>
                <Typography
                  variant="h1"
                  style={{
                    fontFamily: imbPlexSansThai.style.fontFamily,
                    fontWeight: 400,
                    fontSize: "24px",
                    color: "#9CD8D2",
                  }}
                >
                  Welcome back
                </Typography>
              </Box>
            </Box>
          </Box>
        ) : (
          // other pages that not home
          <Box sx={{ display: "flex", gap: "40px", alignItems: "center" }}>
            <Box
              sx={{
                padding: "0px 30px 0px 0px",
              }}
            >
              <Link href={"/home"}>
                <svg
                  width="64"
                  height="64"
                  viewBox="0 0 64 64"
                  fill="none"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <g clipPath="url(#clip0_169_50)">
                    <path
                      d="M1.1748 29.175C-0.387695 30.7375 -0.387695 33.275 1.1748 34.8375L17.1748 50.8375C18.7373 52.4 21.2748 52.4 22.8373 50.8375C24.3998 49.275 24.3998 46.7375 22.8373 45.175L13.6623 36H59.9998C62.2123 36 63.9998 34.2125 63.9998 32C63.9998 29.7875 62.2123 28 59.9998 28H13.6623L22.8373 18.825C24.3998 17.2625 24.3998 14.725 22.8373 13.1625C21.2748 11.6 18.7373 11.6 17.1748 13.1625L1.1748 29.1625V29.175Z"
                      fill="white"
                    />
                  </g>
                  <defs>
                    <clipPath id="clip0_169_50">
                      <rect width="64" height="64" fill="white" />
                    </clipPath>
                  </defs>
                </svg>
              </Link>
            </Box>

            <Typography
              variant="h1"
              style={{
                fontFamily: imbPlexSansThai.style.fontFamily,
                fontWeight: 800,
                fontSize: "64px",
                color: "#ffffff",
              }}
            >
              {titleList[currentPath]}
            </Typography>
          </Box>
        )}
        {/* if it home display notthing else diplay profile and rank */}
        {currentPath === "/home" ? (
          <></>
        ) : (
          <Box sx={{ display: "flex", gap: "20px", alignItems: "center" }}>
            <img
              width="100px"
              height="100px"
              src={Profile().src}
              alt="profileImage"
            />
            
            <Box
              sx={{
                display: "flex",
                flexDirection: "column",
              }}
            >
              <Typography
                variant="h1"
                style={{
                  fontFamily: imbPlexSansThai.style.fontFamily,
                  fontWeight: 700,
                  fontSize: "36px",
                  color: "#ffffff",
                }}
              >
                {username}
              </Typography>
              <Typography
                variant="h1"
                style={{
                  fontFamily: imbPlexSansThai.style.fontFamily,
                  fontWeight: 400,
                  fontSize: "24px",
                  color: "#9CD8D2",
                }}
              >
                Rank NO.{rank}
              </Typography>
            </Box>
          </Box>
        )}

        <Box sx={{ display: "flex", gap: "40px" }}>
          <DayStreak streak={loginStreak} />
          <Link href={"/leagueBoard"}>
            <TotalXp totalXp={totalXp} />
          </Link>
        </Box>
      </Box>
    </AppBar>
  );
}
