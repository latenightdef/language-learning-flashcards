import Box from "@mui/material/Box";
import { Hidden, Typography } from "@mui/material";
import LinearProgress from "@mui/material/LinearProgress";
import { useState } from "react";
import { useRouter } from "next/router";

import useFont from "@/hooks/fonts/useFont";

const { IBM_Plex_Sans_Thai, IBM_Plex_Sans_KR} = useFont()
const imbPlexSansThai = IBM_Plex_Sans_Thai
const imbPlexSansKr = IBM_Plex_Sans_KR

interface DeckProps {
  deckType: string;
  deckId: number;
  deckTitle: string;
  deckMeaning: string;
  progress: number;
}

export default function Deck(props: DeckProps) {
  const [isHovered, setIsHovered] = useState(false);
  // console.log(isHovered);
  const router = useRouter()

  const handleOnClicked = ()=>{
    router.push({pathname:'/learning',query:{deckType:props.deckType,deckId:props.deckId}})
  }

  return (
    <Box
      sx={[
        // when hover
        {
          "&:hover": {
            // width: "183.39px",
            // height: "225.11px",
            border: 5,
            borderColor: "#22A699",
            boxShadow: "0px 15px #007469",
          },
        },
        {
          width: "173.39px",
          height: "225.11px",
          borderRadius: "25px",
          bgcolor: "#ffffff",
          boxShadow: "0px 15px #C8C8C8",
          padding: "10px",
          position: "relative",
          margin: "20px 20px",
          flex: "0 0 auto",
        },
      ]}
      onMouseEnter={() => setIsHovered(true)}
      onMouseLeave={() => setIsHovered(false)}
      onClick={() => handleOnClicked()}
    >
      {/* title and description*/}
      <Box
        sx={{
          paddingTop: "30px",
          display: "flex",
          flexDirection: "column",
          justifyContent: "center",
        }}
      >
        <Typography
          variant="h1"
          style={{
            fontFamily: imbPlexSansKr.style.fontFamily,
            fontWeight: 700,
            fontSize: "64px",
            color: "#303030",
            textAlign: "center",
            textOverflow: 'ellipsis',
            overflow: 'hidden'
          }}
        >
          {props.deckTitle}
        </Typography>
        <Typography
          variant="h1"
          style={{
            fontFamily: imbPlexSansThai.style.fontFamily,
            fontWeight: 400,
            fontSize: "24px",
            color: "#303030",
            textAlign: "center",
            padding: " 0px 20px",
            textOverflow: 'ellipsis',
            overflow: 'hidden'
          }}
        >
          {props.deckMeaning}
        </Typography>
      </Box>
      {/* progress bar */}
      <Box sx={{ position: "absolute", bottom: 15, left: 10, right: 10 }}>
        <LinearProgress
          sx={{
            width: "100%", // Customize the width of the progress bar
            borderRadius: 10, // Customize the border radius
            height: "8px",
            backgroundColor: "#DDDDDD",
            "& .MuiLinearProgress-bar": {
              backgroundColor: "#22A699", // Customize the progress bar color
              borderRadius: 10, // Customize the border radius of the progress bar
            },
          }}
          variant="determinate"
          value={props.progress}
        />
      </Box>
    </Box>
  );
}
