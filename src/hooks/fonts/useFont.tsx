import localFont from "next/font/local";
const IBM_Plex_Sans_Thai = localFont({
  src: [
    {
      path: './fontsFile/IBMPlexSansThai-Bold.ttf',
      weight: '700',
      style: 'normal',
    },
    {
      path: './fontsFile/IBMPlexSansThai-Regular.ttf',
      weight: '400',
      style: 'normal',
    },
  ],
})

const IBM_Plex_Sans_KR = localFont({
  src: [
    {
      path: './fontsFile/IBMPlexSansJP-Bold.ttf',
      weight: '700',
      style: 'normal',
    },
    {
      path: './fontsFile/IBMPlexSansJP-Regular.ttf',
      weight: '400',
      style: 'normal',
    },
  ]
})

const M_PLUS_C1 = localFont({
  src: [
    {
      path: './fontsFile/MPLUSRounded1c-Regular.ttf',
      weight: '400',
      style: 'normal',
    },
    {
      path: './fontsFile/MPLUSRounded1c-Bold.ttf',
      weight: '700',
      style: 'normal',
    },
  ]
})

const useFont = () => {
  return {
    IBM_Plex_Sans_Thai,
    IBM_Plex_Sans_KR,
    M_PLUS_C1,
  };
}

export default useFont