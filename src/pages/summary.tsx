import React, { useState } from 'react';
import { CssBaseline, Container } from '@mui/material';
import AppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import Link from 'next/link';
import Typography from '@mui/material/Typography';
import LinearProgress, { linearProgressClasses } from '@mui/material/LinearProgress';
import { styled } from '@mui/material/styles';
import Button from '@mui/material/Button';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import { useRouter } from "next/router";
import { useEffect } from "react";

import getUserInfo from "@/pages/api/getUserInfo";

import TotalXp from "@/components/TotalXp";
import useAuth from "@/hooks/useAuth";

import useFont from '@/hooks/fonts/useFont';

const { IBM_Plex_Sans_KR } = useFont()
const imbPlexSansKR = IBM_Plex_Sans_KR

let category = "Day of the week ( วันของสัปดาห์ )";

const globalStyles = `
    body {
        background-color: #DDDDDD;
    }
`;

export default function Document(){
  const router = useRouter()

  const [nowNumber,setNowNumber] = useState(1);

  const { setAuth,auth } = useAuth()
  const totalXp = auth.totalXp

  const [easyClick,setEasyClicked] = useState(10) ;
  const [hardClick,setHardClicked] = useState(4) ;
  const [skipClick,setSkipClicked] = useState(8) ;

  useEffect(()=>{
    setEasyClicked(Number(router.query.easyClicked))
    setHardClicked(Number(router.query.hardClicked))
    setSkipClicked(Number(router.query.skipClicked))
  },[router.query]);

  useEffect(()=>{
    getUserInfo().then(
      (res) => {
        if(res.status === 200){
          setAuth(res.data)
          console.log(res.data)
        }
      }
    )
  },[])

  const handleContinue = () =>{
    router.push('/home')
  }

  return (
    <>
      <style>{globalStyles}</style>
      <CssBaseline />
      <Box 
        position="relative"
        sx={{
          background: "#DDDDDD",
          width: "100vw",
          height: "100vh",
        }}
      >
        <AppBar
          position="static"
          sx={{
            backgroundColor: "#22A699",
            borderRadius: "0px 0px  50px 50px",
            padding: "30px 60px 150px 60px",
            boxShadow:0,
          }}
        >
          <Box sx={{position:"relative", display: "flex", gap: "40px", alignItems: "center", zIndex: 1 }}>
            <Box
              sx={{
                padding: "0px 30px 0px 0px",
              }}
            >
              <Link href={"/home"}>
                <svg
                  width="64"
                  height="64"
                  viewBox="0 0 64 64"
                  fill="none"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <g clipPath="url(#clip0_169_50)">
                    <path
                      d="M1.1748 29.175C-0.387695 30.7375 -0.387695 33.275 1.1748 34.8375L17.1748 50.8375C18.7373 52.4 21.2748 52.4 22.8373 50.8375C24.3998 49.275 24.3998 46.7375 22.8373 45.175L13.6623 36H59.9998C62.2123 36 63.9998 34.2125 63.9998 32C63.9998 29.7875 62.2123 28 59.9998 28H13.6623L22.8373 18.825C24.3998 17.2625 24.3998 14.725 22.8373 13.1625C21.2748 11.6 18.7373 11.6 17.1748 13.1625L1.1748 29.1625V29.175Z"
                      fill="white"
                    />
                  </g>
                  <defs>
                    <clipPath id="clip0_169_50">
                      <rect width="64" height="64" fill="white" />
                    </clipPath>
                  </defs>
                </svg>
              </Link>  
            </Box>
            <Box>
              <Typography
                variant="h1"
                style={{
                    fontFamily: imbPlexSansKR.style.fontFamily,
                    fontWeight: 700,
                    fontSize: "48px",
                    color: "#ffffff",
                }}
              >
                Deck learning complete!
              </Typography>
              <Typography
                variant="h1"
                style={{
                    fontFamily: imbPlexSansKR.style.fontFamily,
                    fontWeight: 400,
                    fontSize: "32px",
                    color: "#9CD8D2",
                }}
              >
                {category}
              </Typography>
            </Box>
          </Box>
        </AppBar>

        <Box 
          position="absolute"
          sx={{
            borderRadius: "50px 50px  50px 50px",
            background:"#FFFFFF",
            width:"893.82px",
            height:"226px",
            marginTop:"-100px",
            marginLeft:"512px",
            zIndex: 2, // This sets the box below the AppBar
            display: "flex", 
            alignItems: "center",
            justifyContent:"center",
          }}
        >
            <Typography 
                    variant="h1"
                    style={{
                        fontFamily: imbPlexSansKR.style.fontFamily,
                        fontWeight: 700,
                        fontSize: "96px",
                        color: "#303030",
                    }}
                >
                    Congratulations!
            </Typography>
        </Box>
        {/* shadow */}
        <Box 
          position="absolute"
          sx={{
            borderRadius: "50px 50px  50px 50px",
            background:"#B1B1B1",
            width:"893.82px",
            height:"226px",
            marginTop:"-86px",
            marginLeft:"512px",
            zIndex: 1, // This sets the box below the AppBar
          }}
        />

        <Box sx={{
            marginTop:"176px",
            marginLeft:"805px",
        }}>
            <TotalXp totalXp={totalXp+100} />
        </Box>
        

        <Box 
          position="absolute"
          sx={{
            borderRadius: "25px 25px  25px 25px",
            background:"#FFFFFF",
            width:"123px",
            height:"74.72px",
            marginTop:"26px",
            marginLeft:"581px",
            zIndex: 2, // This sets the box below the AppBar
            display: "flex", 
            alignItems: "center",
            justifyContent:"center",
          }}
        >
            <Typography 
                    variant="h1"
                    style={{
                        fontFamily: imbPlexSansKR.style.fontFamily,
                        fontWeight: 700,
                        fontSize: "48px",
                        color: "#303030",
                    }}
                >
                    {easyClick}
            </Typography>
        </Box>
        {/* shadow */}
        <Box 
          position="absolute"
          sx={{
            borderRadius: "25px 25px  25px 25px",
            background:"#B1B1B1",
            width:"123px",
            height:"74.72px",
            marginTop:"33.28px",
            marginLeft:"581px",
            zIndex: 1, // This sets the box below the AppBar
          }}
        />

        <Box 
          position="absolute"
          sx={{
            borderRadius: "25px 25px  25px 25px",
            background:"#FFFFFF",
            width:"123px",
            height:"74.72px",
            marginTop:"26px",
            marginLeft:"898px",
            zIndex: 2, // This sets the box below the AppBar
            display: "flex", 
            alignItems: "center",
            justifyContent:"center",
          }}
        >
            <Typography 
                    variant="h1"
                    style={{
                        fontFamily: imbPlexSansKR.style.fontFamily,
                        fontWeight: 700,
                        fontSize: "48px",
                        color: "#303030",
                    }}
                >
                    {hardClick}
            </Typography>
        </Box>
        {/* shadow */}
        <Box 
          position="absolute"
          sx={{
            borderRadius: "25px 25px  25px 25px",
            background:"#B1B1B1",
            width:"123px",
            height:"74.72px",
            marginTop:"33.28px",
            marginLeft:"898px",
            zIndex: 1, // This sets the box below the AppBar
          }}
        />

        <Box 
          position="absolute"
          sx={{
            borderRadius: "25px 25px  25px 25px",
            background:"#FFFFFF",
            width:"123px",
            height:"74.72px",
            marginTop:"26px",
            marginLeft:"1215px",
            zIndex: 2, // This sets the box below the AppBar
            display: "flex", 
            alignItems: "center",
            justifyContent:"center",
          }}
        >
            <Typography 
                    variant="h1"
                    style={{
                        fontFamily: imbPlexSansKR.style.fontFamily,
                        fontWeight: 700,
                        fontSize: "48px",
                        color: "#303030",
                    }}
                >
                    {skipClick}
            </Typography>
        </Box>
        {/* shadow */}
        <Box 
          position="absolute"
          sx={{
            borderRadius: "25px 25px  25px 25px",
            background:"#B1B1B1",
            width:"123px",
            height:"74.72px",
            marginTop:"33.28px",
            marginLeft:"1215px",
            zIndex: 1, // This sets the box below the AppBar
          }}
        />

        <Box sx={{
              display:"flex",
              marginTop: "81px",
              marginLeft:"507px"
            }}>
              <Button
                  variant="contained"
                  sx={{ 
                      color: 'black',
                      background: '#22A699',
                      boxShadow: 1,
                      width:"271px",
                      height:"129px",
                      borderRadius: "25px 25px  25px 25px",
                      marginRight:"46px",
                      '&:hover': {
                          backgroundColor: '#007469',
                      },
                  }}
              >
                  <Typography
                      variant="h1"
                      style={{
                          fontFamily: imbPlexSansKR.style.fontFamily,
                          fontWeight: 700,
                          fontSize: "48px",
                          color: "#FFFFFF",
                      }}
                  >
                      Easy
                  </Typography>
              </Button>

              <Button
                  variant="contained"
                  sx={{ 
                      color: 'black',
                      background: '#F29727',
                      boxShadow: 1,
                      width:"271px",
                      height:"129px",
                      borderRadius: "25px 25px  25px 25px",
                      marginRight:"46px",
                      '&:hover': {
                          backgroundColor: '#AB5E00',
                      },
                  }}
              >
                  <Typography
                      variant="h1"
                      style={{
                          fontFamily: imbPlexSansKR.style.fontFamily,
                          fontWeight: 700,
                          fontSize: "48px",
                          color: "#FFFFFF",
                      }}
                  >
                      Hard
                  </Typography>
              </Button>

              <Button
                  variant="contained"
                  sx={{ 
                      color: 'black',
                      background: '#F24C3D',
                      boxShadow: 1,
                      width:"271px",
                      height:"129px",
                      borderRadius: "25px 25px  25px 25px",
                      marginRight:"46px",
                      '&:hover': {
                          backgroundColor: '#B01103',
                      },
                  }}
              >
                  <Typography
                      variant="h1"
                      style={{
                          fontFamily: imbPlexSansKR.style.fontFamily,
                          fontWeight: 700,
                          fontSize: "48px",
                          color: "#FFFFFF",
                      }}
                  >
                      Skip
                  </Typography>
              </Button>
        </Box>

        <Button 
                variant="contained"
                sx={{ 
                    color: 'black',
                    background: 'white',
                    boxShadow: 1,
                    width:"343px",
                    height:"121px",
                    marginLeft: "788px",
                    marginTop: "36.5px",
                    borderRadius: "25px 25px  25px 25px",
                    '&:hover': {
                        backgroundColor: 'grey',
                    },
                }}
                onClick={handleContinue}
            >
                <Typography
                    variant="h1"
                    style={{
                        fontFamily: imbPlexSansKR.style.fontFamily,
                        fontWeight: 700,
                        fontSize: "48px",
                        color: "#303030",
                    }}
                >
                    CONTINUE
                </Typography>
            </Button>
      </Box>
    </>
  );
};
