import Navbar from "@/components/navBar";
import { Typography } from "@mui/material";
import Box from "@mui/material/Box";
import Deck from "@/components/deck";
import AddDeck from "@/components/addDeck";
import React, { useRef, useEffect, useState } from "react";
import Arrow from "../assets/nextArrow.png";
import Link from "next/link";

import profilePicture from "@/assets/profile1.png";
import Order1Picture from "@/assets/Order/1.png";
import Order2Picture from "@/assets/Order/2.png";
import Order3Picture from "@/assets/Order/3.png";

import profile1 from "../assets/Profiles/profile_1.png";
import profile2 from "../assets/Profiles/profile_2.png";
import profile3 from "../assets/Profiles/profile_3.png";
import profile4 from "../assets/Profiles/profile_4.png";
import profile5 from "../assets/Profiles/profile_5.png";
import profile6 from "../assets/Profiles/profile_6.png";
import profile7 from "../assets/Profiles/profile_7.png";

import useFont from "@/hooks/fonts/useFont";

import getFindAllStarterDeck from "./api/getFindStarterDeck";
import getFindAllUserDeck from "./api/getFindAllUserDeck";
import getLeaderBoard from "./api/getLeaderBoard";

const { IBM_Plex_Sans_Thai } = useFont()

let lengthUsers = 0

export default function Document() {
  const containerRef1 = useRef<HTMLDivElement | null>(null);
  const containerRef2 = useRef<HTMLDivElement | null>(null);
  const [starterDeck, setStarterDeck] = useState(<></>);
  const [customDeck, setCustomDeck] = useState(<></>);
  const [users,setUsers] = useState([{username:"",totalExp:0},{username:"",totalExp:0},{username:"",totalExp:0}])

  useEffect(() => {
    const handleOverflow = () => {
      const element1 = containerRef1.current;
      const element2 = containerRef2.current;

      if (element1 && element2) {
        if (
          element1.scrollHeight == element1.clientHeight &&
          element1.scrollWidth == element1.clientWidth
        ) {
          element1.style.overflow = "hidden";
          element1.style.overflowX = "hidden";
          element1.style.paddingBottom = "20px";
          element2.style.paddingBottom = "20px";
        } else {
          element1.style.overflow = "hidden";
          element1.style.overflowX = "scroll";
          element1.style.paddingBottom = "30px";
          element2.style.paddingBottom = "50px";
        }
      }
    };
    handleOverflow();
    handleOverflow();
    window.addEventListener("resize", handleOverflow);
    // Cleanup the event listener when the component is unmounted
    return () => {
      window.removeEventListener("resize", handleOverflow);
    };
  }, []);

  useEffect(() => {
    getFindAllStarterDeck().then((res) => {
      // console.log(res.decks)
      if (res.decks == null) {
        console.log("deck empty");
      } else {
        setStarterDeck(
          res.decks.map((deck: {
            experince: any;
            maxExperince: any;
            nameInLanguage: string;
            nameEng: string; 
            id: any; 
          }) => {
            return <Deck 
            key={deck.id}
            deckType="starter"
            deckId={deck.id}
            deckTitle={deck.nameInLanguage} 
            deckMeaning={deck.nameEng} 
            progress={Math.floor(deck.experince / deck.maxExperince)} 
            />;
          })
        );
      }
    });

    getFindAllUserDeck().then((res)=> {
      console.log("all user deck")
      console.log(res.decks)
      if (res.decks == null) {
        console.log("deck empty");
      } else {
        setCustomDeck(
          res.decks.map((deck: {
            experince: any;
            maxExperince: any;
            nameInLanguage: string;
            nameEng: string; 
            id: any; 
          }) => {
            return <Deck 
            key={deck.id}
            deckType="custom"
            deckId={deck.id}
            deckTitle={deck.nameInLanguage} 
            deckMeaning={deck.nameEng} 
            progress={Math.floor(deck.experince / deck.maxExperince)} 
            />;
          })
        );
      }
    });

    getLeaderBoard(3).then((res)=>{
      lengthUsers = res.ListUsers.length
      setUsers(res.ListUsers)
      // console.log(Array(res.ListUsers))
  })
  }, []);

  const Profile =(user:any)=>{
    if(user?.profileImage=="profile1"){
        return profile1
    }else if(user?.profileImage=="profile2"){
        return profile2
    }else if(user?.profileImage=="profile3"){
        return profile3
    }else if(user?.profileImage=="profile4"){
        return profile4
    }else if(user?.profileImage=="profile5"){
        return profile5
    }else if(user?.profileImage=="profile6"){
        return profile6
    }else{
        return profile7
    }
}

  return (
    <Box sx={{ bgcolor: "#ffffff" }}>
      <Navbar />
      <Box sx={{ padding: "20px" }}>
        <Link href={"/decks"}>
          <Box sx={{ display: "flex", alignItems: "center" }}>
            <h1
              style={{
                fontFamily: IBM_Plex_Sans_Thai.style.fontFamily,
                fontWeight: "700",
                fontSize: "48px",
                color: "#303030",
                padding: "0px 20px 0px 20px",
              }}
            >
              Learn Now
            </h1>
            <img src={Arrow.src} alt="arrow" />
          </Box>
        </Link>

        <Box
          sx={{
            bgcolor: "#DDDDDD",
            margin: "20px",
            padding: "30px 40px 22px 40px",
            borderRadius: "25px",
            display: "flex",
            flexDirection: "row",
            justifyContent: "space-between",
            // gap: "10px",
          }}
        >
          {/* deck */}
          <Box
            sx={{
              display: "flex",
              flexDirection: "row",
              "&::-webkit-scrollbar": {
                width: "0.4em",
              },
              "&::-webkit-scrollbar-track": {
                boxShadow: "inset 0 0 20px #FFFFFF",
                webkitBoxShadow: "inset 0 0 20px #FFFFFF",
                borderRadius: "15px",
                // width: "2px",
              },
              "&::-webkit-scrollbar-thumb": {
                backgroundColor: "#303030",
                borderRadius: "15px",
                width: "2px",
                // outline: '1px solid slategrey'
              },
            }}
            ref={containerRef1}
          >
            {starterDeck}
            {customDeck}
          </Box>

          {/* add deck btn */}
          <Box
            ref={containerRef2}
            sx={{ paddingLeft: "40px", alignItems: "center", display: "flex" }}
          >
            <AddDeck deckTitle={"Add"} deckMeaning={""} progress={0} />
          </Box>
        </Box>
      </Box>
      <Box sx={{ padding: "20px" }}>
        <Link href={"/leagueBoard"}>
          <Box sx={{ display: "flex", alignItems: "center" }}>
            <Typography
              variant="h1"
              style={{
                fontFamily: IBM_Plex_Sans_Thai.style.fontFamily,
                fontWeight: 700,
                fontSize: "48px",
                color: "#303030",
                padding: "0px 20px 0px 20px",
              }}
            >
              League board
            </Typography>
            <img src={Arrow.src} alt="arrow" />
          </Box>
        </Link>

        <Box
          sx={{
            bgcolor: "#DDDDDD",
            margin: "20px",
            padding: "30px 40px 22px 40px",
            borderRadius: "25px",
            display: "flex",
            flexDirection: "column",
            justifyContent: "space-between",
            // gap: "10px",
          }}
        >
          {lengthUsers >= 1 ? (
            <Box
              position="relative"
              sx={{
                bgcolor: "#007469",
                height: "99px",
                width: "1744px",
                marginBottom: "22px",
                borderRadius: "25px 25px 25px 25px",
                display: "flex",
                justifyContent: "space-around",
                alignItems: "center",
              }}
            >
              <img
                width="63px"
                height="63px"
                src={Order1Picture.src}
                alt="orderImage"
              />
              <img
                width="66px"
                height="66px"
                src={Profile(users[0]).src}
                alt="profileImage"
              />
              <Typography
                variant="h1"
                style={{
                  fontFamily: IBM_Plex_Sans_Thai.style.fontFamily,
                  fontWeight: 700,
                  fontSize: "46px",
                  color: "#ffffff",
                  width: "800px",
                }}
              >
                {users[0].username}
              </Typography>
              <svg
                width="43"
                height="57"
                viewBox="0 0 43 57"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
              >
                <g clipPath="url(#clip0_134_449)">
                  <path
                    d="M0 28.5L3.15707 3.11719C3.37861 1.33594 4.88514 0 6.67969 0H25.3562C27.0178 0 28.3582 1.34707 28.3582 3.01699C28.3582 3.37324 28.2917 3.74062 28.1699 4.07461L23.041 17.8125H38.4719C40.7095 17.8125 42.5373 19.6383 42.5373 21.8982C42.5373 22.7221 42.2936 23.5236 41.8284 24.2027L20.5375 55.4859C19.884 56.4434 18.8095 57.0111 17.6685 57.0111H17.3472C15.6081 57.0111 14.1902 55.5861 14.1902 53.8383C14.1902 53.5822 14.2234 53.3262 14.2899 53.0701L19.4963 32.0625H3.54478C1.58407 32.0625 0 30.4705 0 28.5Z"
                    fill="#F29727"
                  />
                </g>
                <defs>
                  <clipPath id="clip0_134_449">
                    <rect width="42.5373" height="57" fill="white" />
                  </clipPath>
                </defs>
              </svg>
              <Box
                sx={{
                  display: "flex",
                  flexDirection: "row",
                  alignItems: "center",
                }}
              >
                <Typography
                  variant="h1"
                  style={{
                    fontFamily: IBM_Plex_Sans_Thai.style.fontFamily,
                    fontWeight: 700,
                    fontSize: "46px",
                    color: "#ffffff",
                  }}
                >
                  {users[0].totalExp}
                </Typography>
                <Typography
                  variant="h1"
                  style={{
                    fontFamily: IBM_Plex_Sans_Thai.style.fontFamily,
                    fontWeight: 400,
                    fontSize: "46px",
                    color: "#ffffff",
                    marginLeft: "20px",
                  }}
                >
                  Total XP
                </Typography>
              </Box>
            </Box>
          ) : <></>}
          {lengthUsers >= 2 ? (
            <Box
              position="relative"
              sx={{
                bgcolor: "#007469",
                height: "99px",
                width: "1744px",
                marginBottom: "22px",
                borderRadius: "25px 25px 25px 25px",
                display: "flex",
                justifyContent: "space-around",
                alignItems: "center",
              }}
            >
              <img
                width="63px"
                height="63px"
                src={Order2Picture.src}
                alt="orderImage"
              />
              <img
                width="66px"
                height="66px"
                src={Profile(users[1]).src}
                alt="profileImage"
              />
              <Typography
                variant="h1"
                style={{
                  fontFamily: IBM_Plex_Sans_Thai.style.fontFamily,
                  fontWeight: 700,
                  fontSize: "46px",
                  color: "#ffffff",
                  width: "800px",
                }}
              >
                {users[1].username}
              </Typography>
              <svg
                width="43"
                height="57"
                viewBox="0 0 43 57"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
              >
                <g clipPath="url(#clip0_134_449)">
                  <path
                    d="M0 28.5L3.15707 3.11719C3.37861 1.33594 4.88514 0 6.67969 0H25.3562C27.0178 0 28.3582 1.34707 28.3582 3.01699C28.3582 3.37324 28.2917 3.74062 28.1699 4.07461L23.041 17.8125H38.4719C40.7095 17.8125 42.5373 19.6383 42.5373 21.8982C42.5373 22.7221 42.2936 23.5236 41.8284 24.2027L20.5375 55.4859C19.884 56.4434 18.8095 57.0111 17.6685 57.0111H17.3472C15.6081 57.0111 14.1902 55.5861 14.1902 53.8383C14.1902 53.5822 14.2234 53.3262 14.2899 53.0701L19.4963 32.0625H3.54478C1.58407 32.0625 0 30.4705 0 28.5Z"
                    fill="#F29727"
                  />
                </g>
                <defs>
                  <clipPath id="clip0_134_449">
                    <rect width="42.5373" height="57" fill="white" />
                  </clipPath>
                </defs>
              </svg>
              <Box
                sx={{
                  display: "flex",
                  flexDirection: "row",
                  alignItems: "center",
                }}
              >
                <Typography
                  variant="h1"
                  style={{
                    fontFamily: IBM_Plex_Sans_Thai.style.fontFamily,
                    fontWeight: 700,
                    fontSize: "46px",
                    color: "#ffffff",
                  }}
                >
                  {users[1].totalExp}
                </Typography>
                <Typography
                  variant="h1"
                  style={{
                    fontFamily: IBM_Plex_Sans_Thai.style.fontFamily,
                    fontWeight: 400,
                    fontSize: "46px",
                    color: "#ffffff",
                    marginLeft: "20px",
                  }}
                >
                  Total XP
                </Typography>
              </Box>
            </Box>
          ) : <></>}
          {lengthUsers >= 3 ? (
            <Box
              position="relative"
              sx={{
                bgcolor: "#007469",
                height: "99px",
                width: "1744px",
                marginBottom: "22px",
                borderRadius: "25px 25px 25px 25px",
                display: "flex",
                justifyContent: "space-around",
                alignItems: "center",
              }}
            >
              <img
                width="63px"
                height="63px"
                src={Order3Picture.src}
                alt="orderImage"
              />
              <img
                width="66px"
                height="66px"
                src={Profile(users[2]).src}
                alt="profileImage"
              />
              <Typography
                variant="h1"
                style={{
                  fontFamily: IBM_Plex_Sans_Thai.style.fontFamily,
                  fontWeight: 700,
                  fontSize: "46px",
                  color: "#ffffff",
                  width: "800px",
                }}
              >
                {users[2].username}
              </Typography>
              <svg
                width="43"
                height="57"
                viewBox="0 0 43 57"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
              >
                <g clipPath="url(#clip0_134_449)">
                  <path
                    d="M0 28.5L3.15707 3.11719C3.37861 1.33594 4.88514 0 6.67969 0H25.3562C27.0178 0 28.3582 1.34707 28.3582 3.01699C28.3582 3.37324 28.2917 3.74062 28.1699 4.07461L23.041 17.8125H38.4719C40.7095 17.8125 42.5373 19.6383 42.5373 21.8982C42.5373 22.7221 42.2936 23.5236 41.8284 24.2027L20.5375 55.4859C19.884 56.4434 18.8095 57.0111 17.6685 57.0111H17.3472C15.6081 57.0111 14.1902 55.5861 14.1902 53.8383C14.1902 53.5822 14.2234 53.3262 14.2899 53.0701L19.4963 32.0625H3.54478C1.58407 32.0625 0 30.4705 0 28.5Z"
                    fill="#F29727"
                  />
                </g>
                <defs>
                  <clipPath id="clip0_134_449">
                    <rect width="42.5373" height="57" fill="white" />
                  </clipPath>
                </defs>
              </svg>
              <Box
                sx={{
                  display: "flex",
                  flexDirection: "row",
                  alignItems: "center",
                }}
              >
                <Typography
                  variant="h1"
                  style={{
                    fontFamily: IBM_Plex_Sans_Thai.style.fontFamily,
                    fontWeight: 700,
                    fontSize: "46px",
                    color: "#ffffff",
                  }}
                >
                  {users[2].totalExp}
                </Typography>
                <Typography
                  variant="h1"
                  style={{
                    fontFamily: IBM_Plex_Sans_Thai.style.fontFamily,
                    fontWeight: 400,
                    fontSize: "46px",
                    color: "#ffffff",
                    marginLeft: "20px",
                  }}
                >
                  Total XP
                </Typography>
              </Box>
            </Box>
          ) : <></>}
        </Box>
      </Box>
    </Box>
  );
}
