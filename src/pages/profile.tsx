import * as React from 'react';
import { useEffect, useState } from "react";
import {Button, Box, Typography, Grid, styled , Modal, TextField, ImageList, ImageListItem , Alert , AlertTitle} from '@mui/material';
import useAuth from "@/hooks/useAuth";
import Image from "next/image";
import profile1 from "../assets/Profiles/profile_1.png";
import profile2 from "../assets/Profiles/profile_2.png";
import profile3 from "../assets/Profiles/profile_3.png";
import profile4 from "../assets/Profiles/profile_4.png";
import profile5 from "../assets/Profiles/profile_5.png";
import profile6 from "../assets/Profiles/profile_6.png";
import profile7 from "../assets/Profiles/profile_7.png";
import changeIcon from "../assets/changeIcon.png";
import logoutIcon from "../assets/logOut.png";
import useFont from "@/hooks/fonts/useFont";

import Navbar from "@/components/navBar";
import { Password } from '@mui/icons-material';

import getUserInfo from "@/pages/api/getUserInfo";
import postUserEdit from './api/postUserEdit';
import postUserProfileEdit from './api/postUserProfileEdit';
import { useRouter } from 'next/router';

const { IBM_Plex_Sans_Thai } = useFont()

const EditprofileButton = styled(Button)({
    textTransform: 'capitalize', 
    borderRadius: "12px", 
    backgroundColor:"#F39201", 
    fontWeight: 400, 
    fontSize:"20px",
    '&:hover':{
        backgroundColor: '#F39201',
    }
});
const LogoutButton = styled(Button)({
    textTransform: 'capitalize', 
    borderRadius: "12px", 
    backgroundColor:"#F24C3D", 
    fontWeight: 400, 
    fontSize:"20px",
    '&:hover':{
        backgroundColor: '#F24C3D',
    }
});

const TextFont = styled(Typography)({
    fontFamily: IBM_Plex_Sans_Thai.style.fontFamily,
    fontWeight: 700,
});

const BoxImage = styled(Box)({
    borderRadius: "50%", 
    // border: "7px solid #F39201",
});


let itemImage = [
    {
        img: {profile1},
        title: 'profile1',
    },
    {
        img: profile2,
        title: 'profile2',
    },
    {
        img: profile3,
        title: 'profile3',
    },
    {
        img: profile4,
        title: 'profile4',
    },
    {
        img: profile5,
        title: 'profile5',
    },
    {
        img: profile6,
        title: 'profile6',
    },
];


export default function Profile() {
    const router = useRouter()
    const { setAuth,auth } = useAuth()
    const [name,setName] = useState({
        userName: auth.username,
        nickName: auth.nickname,
        password: "123456789",
        profile: auth.profileImage
    });

    useEffect(()=>{
        getUserInfo().then(
          (res) => {
            if(res.status === 200){
              setAuth(res.data)
              console.log(res.data)
            }
          }
        )
    },[])

    const [modalEditProfileState,setModalEditProfileState] = useState(false)
    const [modalChangeIconState,setModalChangeIconState] = useState(false)

    const [profileImage,setProfileImage] = useState("")
    const [profile1Border,setProfile1Border] = useState("")
    const [profile2Border,setProfile2Border] = useState("")
    const [profile3Border,setProfile3Border] = useState("")
    const [profile4Border,setProfile4Border] = useState("")
    const [profile5Border,setProfile5Border] = useState("")
    const [profile6Border,setProfile6Border] = useState("")
    const [editUsername, setEditUsername] = React.useState(name.userName);
    const [editNickname, setEditNickname] = React.useState(name.nickName);
    const [oldPassword, setOldPassword]   = React.useState(name.password);
    const [editPassword, setEditPassword] = React.useState("");
    const [confirmPassword, setConfirmPassword] = React.useState("");

    const [editProfileAlert,setEditProfileAlert] = useState(<></>)

    const Profile =()=>{
        if(name.profile=="profile1"){
            return profile1
        }else if(name.profile=="profile2"){
            return profile2
        }else if(name.profile=="profile3"){
            return profile3
        }else if(name.profile=="profile4"){
            return profile4
        }else if(name.profile=="profile5"){
            return profile5
        }else if(name.profile=="profile6"){
            return profile6
        }else{
            return profile7
        }
    }

    const handleProfile1Clicked=()=>{
        if(profile1Border==""){
            setProfile1Border("7px solid #F39201")
            setProfile2Border("")
            setProfile3Border("")
            setProfile4Border("")
            setProfile5Border("")
            setProfile6Border("")
            setProfileImage("profile1")
        }
        else{
            setProfile1Border("")
            setProfile2Border("")
            setProfile3Border("")
            setProfile4Border("")
            setProfile5Border("")
            setProfile6Border("")
            setProfileImage("")
        }
    }
    const handleProfile2Clicked=()=>{
        if(profile2Border==""){
            setProfile1Border("")
            setProfile2Border("7px solid #F39201")
            setProfile3Border("")
            setProfile4Border("")
            setProfile5Border("")
            setProfile6Border("")
            setProfileImage("profile2")
        }
        else{
            setProfile1Border("")
            setProfile2Border("")
            setProfile3Border("")
            setProfile4Border("")
            setProfile5Border("")
            setProfile6Border("")
            setProfileImage("")
        }
    }
    const handleProfile3Clicked=()=>{
        if(profile3Border==""){
            setProfile1Border("")
            setProfile2Border("")
            setProfile3Border("7px solid #F39201")
            setProfile4Border("")
            setProfile5Border("")
            setProfile6Border("")
            setProfileImage("profile3")
        }
        else{
            setProfile1Border("")
            setProfile2Border("")
            setProfile3Border("")
            setProfile4Border("")
            setProfile5Border("")
            setProfile6Border("")
            setProfileImage("")
        }
    }
    const handleProfile4Clicked=()=>{
        if(profile4Border==""){
            setProfile1Border("")
            setProfile2Border("")
            setProfile3Border("")
            setProfile4Border("7px solid #F39201")
            setProfile5Border("")
            setProfile6Border("")
            setProfileImage("profile4")
        }
        else{
            setProfile1Border("")
            setProfile2Border("")
            setProfile3Border("")
            setProfile4Border("")
            setProfile5Border("")
            setProfile6Border("")
            setProfileImage("")
        }
    }
    const handleProfile5Clicked=()=>{
        if(profile5Border==""){
            setProfile1Border("")
            setProfile2Border("")
            setProfile3Border("")
            setProfile4Border("")
            setProfile5Border("7px solid #F39201")
            setProfile6Border("")
            setProfileImage("profile5")
        }
        else{
            setProfile1Border("")
            setProfile2Border("")
            setProfile3Border("")
            setProfile4Border("")
            setProfile5Border("")
            setProfile6Border("")
            setProfileImage("")
        }
    }
    const handleProfile6Clicked=()=>{
        if(profile6Border==""){
            setProfile1Border("")
            setProfile2Border("")
            setProfile3Border("")
            setProfile4Border("")
            setProfile5Border("")
            setProfile6Border("7px solid #F39201")
            setProfileImage("profile6")
        }
        else{
            setProfile1Border("")
            setProfile2Border("")
            setProfile3Border("")
            setProfile4Border("")
            setProfile5Border("")
            setProfile6Border("")
            setProfileImage("")
        }
    }

    const handleClickEditProfile = ()=>{
        setEditUsername(name.userName)
        setEditNickname(name.nickName)
        setOldPassword(name.password)
        setModalEditProfileState(!modalEditProfileState)
    }
    const handleClickChangeIcon = ()=>{
        setName({
            userName: editUsername,
            nickName: editNickname,
            password: editPassword,
            profile: profileImage
        })

        postUserProfileEdit(profileImage).then((res)=>{
            console.log(res.status)
        })

        setModalChangeIconState(!modalChangeIconState)
    }

    const handleSaveEdit=()=>{
        if((confirmPassword==editPassword)&&(confirmPassword!="")){
            setEditProfileAlert(<></>)
            setName({
                userName: editUsername,
                nickName: editNickname,
                password: editPassword,
                profile: name.profile
            })

            postUserEdit(editUsername,oldPassword,editPassword,editNickname).then((res)=>{
                console.log(res.status)
            })

            handleClickEditProfile()

            // when not reload navbar username doesn't change when change username
            // when reload page api doesn't get userinfo 
            // window.location.reload();
        }
        else{
            setEditProfileAlert(
                <Alert severity="error">
                <AlertTitle>Error</AlertTitle>
                password and confirm password doesn't match! or Haven't entered a password yet
                </Alert>
            )
        }
    }

    const handleSavePhoto=()=>{

    }

    const handleSignOut=()=>{
        localStorage.clear()

        router.push('/')
    }

    return(
        <>
        <Box sx={{backgroundColor:"#FFFFFF", minHeight:"100vh", minWidth:"100vw"}}>
            <Navbar/>
            
                {/* Picture and name */}
                <Box display="grid" gridTemplateColumns="repeat(2, 1fr)" gap={5} sx={{backgroundColor:"", padding:"50px"}}>
                    {/* picture */}
                    <Grid container direction="row" justifyContent="flex-end" alignItems="flex-end">
                        <Box sx={{position:"relative", zIndex:1}}>
                            <Image src={Profile()} width={200} height={200} alt="profileImage"></Image>
                        </Box>
                        <Box sx={{position:"absolute", zIndex:2}}>
                            <Image src={changeIcon} width={50} height={50} alt="changeIcon" onClick={handleClickChangeIcon}></Image>
                        </Box>
                    </Grid>
                    {/* name */}
                    <Grid container direction="column" justifyContent="center" alignItems="flex-start" sx={{backgroundColor:""}}>
                        <TextFont variant="h1" color= "#303030" style={{fontSize:"46px"}}>
                            {name.userName}
                        </TextFont>
                        <Grid container direction="row" gap={1}>
                            <TextFont variant="h1" color="#C8C8C8" style={{fontSize:"20px"}}>
                                add caption today 
                            </TextFont>
                            <TextFont variant="h1" color="#303030" style={{fontSize:"20px"}}>
                                edit
                            </TextFont>
                        </Grid>
                    </Grid>
                </Box>

                <Grid container direction="column" justifyContent="center" alignItems="center" sx={{backgroundColor:""}}>
                    <Grid container direction="row" justifyContent="space-between" alignItems="center" sx={{backgroundColor:"", width:"50vw"}}>
                        {/* Username */}
                        <Box color="#303030" sx={{backgroundColor:"", width:"40%"}}>
                            <TextFont variant="h2" color= "#303030" style={{fontSize:"20px", paddingBottom:"5px"}}>
                                Username
                            </TextFont>
                            <Box display="flex" justifyContent="center" sx={{borderRadius: "10px", bgcolor:"#F3F3F3", p:2, border:2}}>
                                <TextFont variant="h2" color= "#303030" style={{fontSize:"30px"}}>
                                    {name.userName}
                                </TextFont>
                            </Box>
                        </Box>

                        {/* Nickname */}
                            <Box color="#303030" sx={{backgroundColor:"", width:"40%"}}>
                                <TextFont variant="h2" color= "#303030" style={{fontSize:"20px", paddingBottom:"5px"}}>
                                    Nickname
                                </TextFont>
                                <Box display="flex" justifyContent="center" sx={{borderRadius: "10px", bgcolor:"#F3F3F3", p:2, border:2}}>
                                    <TextFont variant="h2" color= "#303030" style={{fontSize:"30px"}}>
                                        {name.nickName}
                                    </TextFont>
                                </Box>
                            </Box>
                    </Grid>
                    {/* Password */}
                    <Grid sx={{backgroundColor:"", width:"50vw", py:"60px"}}>
                        <Box sx={{backgroundColor:""}}>
                            <TextFont variant="h2" color= "#303030" style={{fontSize:"20px", paddingBottom:"5px"}}>
                                Password
                            </TextFont>
                            <TextField disabled id="outlined-password-input" variant="outlined" type="password" value={name.password} 
                                sx={{borderRadius: "10px", border: "2px solid black", bgcolor:"#F3F3F3", width:"100%", input:{textAlign:"center", fontSize:"30px"}}}/>
                        </Box>
                    </Grid>
                    <Grid container direction="row" justifyContent="center" alignItems="center" gap={10}>
                        <EditprofileButton variant="contained" onClick={handleClickEditProfile} sx={{mb:"50px", p:2, width: "300px"}}>
                            <TextFont style={{fontSize:"20px"}}>Edit Profile</TextFont>
                        </EditprofileButton>
                        <LogoutButton variant="contained" sx={{mb:"50px", p:2, width: "300px"}} onClick={handleSignOut}>
                            <Grid container direction="row" justifyContent="center" alignItems="center" gap={2}>
                                <TextFont style={{fontSize:"20px"}}>Log Out</TextFont>
                                <Image src={logoutIcon} alt=""></Image>
                            </Grid>
                        </LogoutButton>
                    </Grid>
                    
                </Grid>
                
                

                {/* editProfile */}
                <Modal
                    open={modalEditProfileState}
                    onClose={handleClickEditProfile}
                >
                    <Grid container direction="column" justifyContent="center" alignItems="center" sx={{backgroundColor:"", width:"100vw", height:"100vh"}}>
                        <Grid container direction="column" justifyContent="center" alignItems="center" gap={3} sx={{backgroundColor:"white", width:"30vw", borderRadius: "10px", p:4}}>
                            <TextFont variant="h2" color= "#303030" style={{fontSize:"25px", paddingBottom:"5px"}}>
                                Edit Profile
                            </TextFont>
                            {/* username */}
                            <Box color="#303030" sx={{backgroundColor:"", width:"90%"}}>
                                <TextFont variant="h2" color= "#303030" style={{fontSize:"20px", paddingBottom:"5px"}}>
                                    Username
                                </TextFont>
                                <TextField id="outlined-controlled" value={editUsername} sx={{width:"100%", input:{fontFamily: IBM_Plex_Sans_Thai.style.fontFamily, fontWeight: 700}}}
                                    onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
                                        setEditUsername(event.target.value);
                                    }}>
                                </TextField>
                            </Box>
                            {/* nickname */}
                            <Box color="#303030" sx={{backgroundColor:"", width:"90%"}}>
                                <TextFont variant="h2" color= "#303030" style={{fontSize:"20px", paddingBottom:"5px"}}>
                                    Nickname
                                </TextFont>
                                <TextField id="outlined-controlled" value={editNickname} sx={{width:"100%", input:{fontFamily: IBM_Plex_Sans_Thai.style.fontFamily, fontWeight: 700}}}
                                    onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
                                        setEditNickname(event.target.value);
                                    }}>
                                </TextField>
                            </Box>
                            {/* old password */}
                            <Box color="#303030" sx={{backgroundColor:"", width:"90%"}}>
                                <TextFont variant="h2" color= "#303030" style={{fontSize:"20px", paddingBottom:"5px"}}>
                                    Old Password
                                </TextFont>
                                <TextField id="outlined-password-input" type="password" value={oldPassword} sx={{width:"100%", input:{fontFamily: IBM_Plex_Sans_Thai.style.fontFamily, fontWeight: 700}}}
                                    onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
                                        setOldPassword(event.target.value);
                                    }}>
                                </TextField>
                            </Box>
                            {/* password */}
                            <Box color="#303030" sx={{backgroundColor:"", width:"90%"}}>
                                <TextFont variant="h2" color= "#303030" style={{fontSize:"20px", paddingBottom:"5px"}}>
                                    New Password
                                </TextFont>
                                <TextField id="outlined-password-input" type="password" value={editPassword} sx={{width:"100%", input:{fontFamily: IBM_Plex_Sans_Thai.style.fontFamily, fontWeight: 700}}}
                                    onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
                                        setEditPassword(event.target.value);
                                    }}>
                                </TextField>
                            </Box>
                            {/* confirm password */}
                            <Box color="#303030" sx={{backgroundColor:"", width:"90%"}}>
                                <TextFont variant="h2" color= "#303030" style={{fontSize:"20px", paddingBottom:"5px"}}>
                                    Confirm Password
                                </TextFont>
                                <TextField id="outlined-password-input" type="password" value={confirmPassword} sx={{width:"100%", input:{fontFamily: IBM_Plex_Sans_Thai.style.fontFamily, fontWeight: 700}}}
                                    onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
                                        setConfirmPassword(event.target.value);
                                    }}>
                                </TextField>
                            </Box>

                            {editProfileAlert}

                            <EditprofileButton variant="contained" onClick={handleSaveEdit} sx={{ p:2, width: "300px"}}>
                                <TextFont style={{fontSize:"20px"}}>Save Changes</TextFont>
                            </EditprofileButton>
                        </Grid>
                    </Grid>
                    
                </Modal>
                

                {/* ChangeIcon */}
                <Modal
                    open={modalChangeIconState}
                    onClose={handleClickChangeIcon}
                >
                    <Grid container direction="column" justifyContent="center" alignItems="center" sx={{backgroundColor:"", width:"100vw", height:"100vh"}}>
                        <Grid container direction="column" justifyContent="center" alignItems="center" gap={3} sx={{backgroundColor:"white", width:"700px", borderRadius: "10px", p:4}}>
                            <TextFont variant="h2" color= "#303030" style={{fontSize:"25px", paddingBottom:"5px"}}>
                                Edit Profile Photo
                            </TextFont>
                            <Box display="grid" gridTemplateColumns="repeat(3, 1fr)" gap={5} sx={{pt:"20px", pb:"50px"}}>
                                <BoxImage sx={{backgroundColor:"#7cba4c", border:profile1Border}}>
                                    <Image src={profile1} width={120} height={120} alt="profile1" onClick={handleProfile1Clicked}></Image>
                                </BoxImage>
                                <BoxImage sx={{backgroundColor:"#fba4d3", border:profile2Border}}>
                                    <Image src={profile2} width={120} height={120} alt="profile2" onClick={handleProfile2Clicked}></Image>
                                </BoxImage>
                                <BoxImage sx={{backgroundColor:"#acaaa9", border:profile3Border}}>
                                    <Image src={profile3} width={120} height={120} alt="profile3" onClick={handleProfile3Clicked}></Image>
                                </BoxImage>
                                <BoxImage sx={{backgroundColor:"#dcf3fa", border:profile4Border}}>
                                    <Image src={profile4} width={120} height={120} alt="profile4" onClick={handleProfile4Clicked}></Image>
                                </BoxImage>
                                <BoxImage sx={{backgroundColor:"#86d4fa", border:profile5Border}}>
                                    <Image src={profile5} width={120} height={120} alt="profile5" onClick={handleProfile5Clicked}></Image>
                                </BoxImage>
                                <BoxImage sx={{backgroundColor:"#e36463", border:profile6Border}}>
                                    <Image src={profile6} width={120} height={120} alt="profile6" onClick={handleProfile6Clicked}></Image>
                                </BoxImage>
                            </Box>

                            <EditprofileButton variant="contained" onClick={handleClickChangeIcon} sx={{p:2, width: "300px"}}>
                                <TextFont style={{fontSize:"20px"}}>Save Changes</TextFont>
                            </EditprofileButton>
                        </Grid>
                    </Grid>
                </Modal>
 
        </Box>     
        
        </>
    );
}

