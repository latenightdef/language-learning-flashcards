import React, { useEffect, useState } from 'react';
import { CssBaseline, Container } from '@mui/material';
import AppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import Link from 'next/link';
import Typography from '@mui/material/Typography';
import LinearProgress, { linearProgressClasses } from '@mui/material/LinearProgress';
import { styled } from '@mui/material/styles';
import Button from '@mui/material/Button';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import { useRouter } from "next/router";

import useFont from '@/hooks/fonts/useFont';

import getPlayDeck from "./api/getPlayDeck";
import getPlayStarterDeck from "./api/getPlayStarterDeck";

import getFindAllStarterDeck from './api/getFindStarterDeck';
import getFindAllUserDeck from './api/getFindAllUserDeck';

import postSendDeck from './api/postSendDeck';
import postSendStarterDeck from './api/postSendStarterDeck';

const { IBM_Plex_Sans_KR} = useFont()

const imbPlexSansKR = IBM_Plex_Sans_KR

// let myDeck = [
//   {
//     no: 1,
//     word: "日曜日",
//     pronunce:"にちようび / nichiyoubi / นิจิโยวบิ",
//     translated:"Sunday วันอาทิตย์"
//   },{
//     no: 2,
//     word: "水曜日",
//     pronunce:"すいようび / suiyobi / ซุยโยวบิ",
//     translated:"Wednesday วันพุธ"
//   },{
//     no: 3,
//     word: "木曜日",
//     pronunce:"もくようび / mokuyoubi / โมคุโยวบิ",
//     translated:"Thursday วันพฤหัสบดี"
//   }
// ];

const globalStyles = `
    body {
        background-color: #DDDDDD;
    }
`;

const sendData:any[] = []

export default function Learning(){
  const router = useRouter()

  const [state, setState] = useState("guess");
  //guess reveal 

  const [myDeck,setMyDeck] = useState([
    {
      no: 1,
      word: "日曜日",
      pronunce:"にちようび / nichiyoubi / นิจิโยวบิ",
      translated:"Sunday วันอาทิตย์"
    },{
      no: 2,
      word: "水曜日",
      pronunce:"すいようび / suiyobi / ซุยโยวบิ",
      translated:"Wednesday วันพุธ"
    },{
      no: 3,
      word: "木曜日",
      pronunce:"もくようび / mokuyoubi / โมคุโยวบิ",
      translated:"Thursday วันพฤหัสบดี"
    }
  ])

  const [category,setCategory] = useState("")

  useEffect(() => {
    if(String(router.query.deckType)=="custom"){
      getPlayDeck(Number(router.query.deckId)).then((res)=>{
        if (res.cards == null) {
          console.log("card empty");
        } else {
          console.log("res Card =",res.cards);
          setMyDeck(
            res.cards.map((card: {
              word: string;
              pronunciationLanguage: string;
              pronunciationEng: string;
              pronunciationThai: string;
              translation: string; 
              id: any; 
            }) => {
              return {
                no: card.id,
                word: card.word,
                pronunce: `${card.pronunciationLanguage} / ${card.pronunciationEng} / ${card.pronunciationThai}`,
                translated: card.translation
              };
            })
          );
        }
      });

      getFindAllUserDeck().then((res)=> {
        if (res.decks == null) {
          console.log("deck empty");
        } else {
          for(let i=0;i<res.decks.length;i++){
            if(Number(res.decks[i].id)==Number(router.query.deckId)){
              setCategory(`${res.decks[i].nameInLanguage} ( ${res.decks[i].nameEng} )`)
            }
          }
        }
      })
    } else{
      getPlayStarterDeck(Number(router.query.deckId)).then((res)=>{
        if (res.cards == null) {
          console.log("card empty");
        } else {
          console.log("res Card =",res.cards);
          setMyDeck(
            res.cards.map((card: {
              word: string;
              pronunciationLanguage: string;
              pronunciationEng: string;
              pronunciationThai: string;
              translation: string; 
              id: any; 
            }) => {
              return {
                no: card.id,
                word: card.word,
                pronunce: `${card.pronunciationLanguage} / ${card.pronunciationEng} / ${card.pronunciationThai}`,
                translated: card.translation
              };
            })
          );
        }
      });

      getFindAllStarterDeck().then((res)=> {
        if (res.decks == null) {
          console.log("deck empty");
        } else {
          for(let i=0;i<res.decks.length;i++){
            if(Number(res.decks[i].id)==Number(router.query.deckId)){
              setCategory(`${res.decks[i].nameInLanguage} ( ${res.decks[i].nameEng} )`)
            }
          }
        }
      })
    }
  }, []);
  
  const numWord = (myDeck === undefined ? 0 : myDeck.length);
  const [nowNumber,setNowNumber] = useState(1);
  const [easyClicked,setEasyClicked] = useState(0);
  const [hardClicked,setHardClicked] = useState(0);
  const [skipClicked,setSkipClicked] = useState(0);

  //ProgressBar
  const BorderLinearProgress = styled(LinearProgress)(({ theme }) => ({
    marginLeft: 57,
    marginTop: 70,
    height: 18,
    width: 1806,
    [`&.${linearProgressClasses.colorPrimary}`]: {
      backgroundColor: theme.palette.grey[theme.palette.mode === 'light' ? 200 : 800],
    },
    [`& .${linearProgressClasses.bar}`]: {
      backgroundColor: theme.palette.mode === 'light' ? '#22A699' : '#308fe8',
    },
  }));

  const handleSendData = () =>{
    if(String(router.query.deckType)=="custom"){
      postSendDeck(Number(router.query.deckId),sendData).then((res)=>{
        console.log(res)
      })
    }else{
      postSendStarterDeck(Number(router.query.deckId),sendData).then((res)=>{
        console.log(res)
      })
    }
  }

  const handleReveal = () =>{
    setState("reveal")
  }

  const handleEasy = () =>{
    setState("guess")
    setEasyClicked(easyClicked+1)
    sendData.push({
      id: myDeck[nowNumber-1].no,
      state: "easy"
    });
    

    if(nowNumber<numWord){
      setNowNumber(nowNumber+1)
    }
    else{
      handleSendData()
      router.push({pathname:'/summary',query:{easyClicked:easyClicked+1 ,hardClicked:hardClicked ,skipClicked:skipClicked}})
    }
      
  }
  const handleHard = () =>{
    setState("guess")
    setHardClicked(hardClicked+1)
    sendData.push({
      id: myDeck[nowNumber-1].no,
      state: "hard"
    });
    

    if(nowNumber<numWord){
      setNowNumber(nowNumber+1)
    }
    else{
      handleSendData()
      router.push({pathname:'/summary',query:{easyClicked:easyClicked ,hardClicked:hardClicked+1 ,skipClicked:skipClicked}})
    }
      
  }
  const handleSkip = () =>{
    setState("guess")
    setSkipClicked(skipClicked+1)
    sendData.push({
      id: myDeck[nowNumber-1].no,
      state: "skip"
    });
    

    if(nowNumber<numWord){
      setNowNumber(nowNumber+1)
    }
    else{
      handleSendData()
      router.push({pathname:'/summary',query:{easyClicked:easyClicked ,hardClicked:hardClicked ,skipClicked:skipClicked+1}})
    }
      
  }

  return (
    <>
      <style>{globalStyles}</style>
      <CssBaseline />
      <Box 
        position="relative"
        sx={{
          background: "#DDDDDD",
          width: "100vw",
          height: "100vh",
        }}
      >
        <AppBar
          position="static"
          sx={{
            backgroundColor: "#22A699",
            borderRadius: "0px 0px  50px 50px",
            padding: "30px 60px 150px 60px",
            boxShadow:0,
          }}
        >
          <Box sx={{position:"relative", display: "flex", gap: "40px", alignItems: "center", zIndex: 1 }}>
            <Box
              sx={{
                padding: "0px 30px 0px 0px",
              }}
            >
              <Link href={"/home"}>
                <svg
                  width="64"
                  height="64"
                  viewBox="0 0 64 64"
                  fill="none"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <g clipPath="url(#clip0_169_50)">
                    <path
                      d="M1.1748 29.175C-0.387695 30.7375 -0.387695 33.275 1.1748 34.8375L17.1748 50.8375C18.7373 52.4 21.2748 52.4 22.8373 50.8375C24.3998 49.275 24.3998 46.7375 22.8373 45.175L13.6623 36H59.9998C62.2123 36 63.9998 34.2125 63.9998 32C63.9998 29.7875 62.2123 28 59.9998 28H13.6623L22.8373 18.825C24.3998 17.2625 24.3998 14.725 22.8373 13.1625C21.2748 11.6 18.7373 11.6 17.1748 13.1625L1.1748 29.1625V29.175Z"
                      fill="white"
                    />
                  </g>
                  <defs>
                    <clipPath id="clip0_169_50">
                      <rect width="64" height="64" fill="white" />
                    </clipPath>
                  </defs>
                </svg>
              </Link>  
            </Box>
            <Typography
                variant="h1"
                style={{
                    fontFamily: imbPlexSansKR.style.fontFamily,
                    fontWeight: 700,
                    fontSize: "64px",
                    color: "#ffffff",
                }}
            >
              {nowNumber}/{numWord}
            </Typography>
            <Box>
              <Typography
                variant="h1"
                style={{
                    fontFamily: imbPlexSansKR.style.fontFamily,
                    fontWeight: 700,
                    fontSize: "48px",
                    color: "#ffffff",
                }}
              >
                Learning Japanese
              </Typography>
              <Typography
                variant="h1"
                style={{
                    fontFamily: imbPlexSansKR.style.fontFamily,
                    fontWeight: 400,
                    fontSize: "32px",
                    color: "#9CD8D2",
                }}
              >
                {category}
              </Typography>
            </Box>
          </Box>
        </AppBar>

        <Box 
          position="absolute"
          sx={{
            borderRadius: "50px 50px  50px 50px",
            background:"#FFFFFF",
            width:"723px",
            height:"319px",
            marginTop:"-100px",
            marginLeft:"598px",
            zIndex: 2, // This sets the box below the AppBar
            display: "flex", 
            alignItems: "center",
            justifyContent:"center",
          }}
        >
            {state=="guess" ?
                <Typography 
                    variant="h1"
                    style={{
                        fontFamily: imbPlexSansKR.style.fontFamily,
                        fontWeight: 700,
                        fontSize: "150px",
                        color: "#303030",
                    }}
                >
                    {myDeck[nowNumber-1].word}
                </Typography>
            :
                <Box sx={{
                  display:"flex",
                  flexDirection:"column",
                  alignItems:"center",
                  justifyContent:"center"
                }}>
                  <Typography 
                      variant="h1"
                      style={{
                          fontFamily: imbPlexSansKR.style.fontFamily,
                          fontWeight: 700,
                          fontSize: "64px",
                          color: "#303030",
                      }}
                  >
                      {myDeck[nowNumber-1].word}
                  </Typography>
                  <Typography 
                      variant="h1"
                      style={{
                          fontFamily: imbPlexSansKR.style.fontFamily,
                          fontWeight: 700,
                          fontSize: "24px",
                          color: "#636363",
                      }}
                  >
                      {myDeck[nowNumber-1].pronunce}
                  </Typography>
                  <Typography 
                      variant="h1"
                      style={{
                          fontFamily: imbPlexSansKR.style.fontFamily,
                          fontWeight: 700,
                          fontSize: "48px",
                          color: "#303030",
                          marginTop:"24px"
                      }}
                  >
                      {myDeck[nowNumber-1].translated}
                  </Typography>

                </Box>
            }
        </Box>
        {/* shadow */}
        <Box 
          position="absolute"
          sx={{
            borderRadius: "50px 50px  50px 50px",
            background:"#B1B1B1",
            width:"723px",
            height:"319px",
            marginTop:"-86px",
            marginLeft:"598px",
            zIndex: 1, // This sets the box below the AppBar
          }}
        />
        
        {state=="guess" ?
            <Button 
                variant="contained"
                sx={{ 
                    color: 'black',
                    background: 'white',
                    boxShadow: 1,
                    width:"343px",
                    height:"121px",
                    marginLeft: "788px",
                    marginTop: "363px",
                    borderRadius: "25px 25px  25px 25px",
                    '&:hover': {
                        backgroundColor: '#DDDDDD',
                    },
                }}
                onClick={handleReveal}
            >
                <Typography
                    variant="h1"
                    style={{
                        fontFamily: imbPlexSansKR.style.fontFamily,
                        fontWeight: 700,
                        fontSize: "48px",
                        color: "#303030",
                    }}
                >
                    Reveal
                </Typography>
            </Button>
        :
            <Box sx={{
              display:"flex",
              marginTop: "363px",
              marginLeft:"507px"
            }}>
              <Button 
                  variant="contained"
                  sx={{ 
                      color: 'black',
                      background: '#22A699',
                      boxShadow: 1,
                      width:"271px",
                      height:"129px",
                      borderRadius: "25px 25px  25px 25px",
                      marginRight:"46px",
                      '&:hover': {
                          backgroundColor: '#007469',
                      },
                  }}
                  onClick={handleEasy}
              >
                  <Typography
                      variant="h1"
                      style={{
                          fontFamily: imbPlexSansKR.style.fontFamily,
                          fontWeight: 700,
                          fontSize: "48px",
                          color: "#FFFFFF",
                      }}
                  >
                      Easy
                  </Typography>
              </Button>

              <Button 
                  variant="contained"
                  sx={{ 
                      color: 'black',
                      background: '#F29727',
                      boxShadow: 1,
                      width:"271px",
                      height:"129px",
                      borderRadius: "25px 25px  25px 25px",
                      marginRight:"46px",
                      '&:hover': {
                          backgroundColor: '#AB5E00',
                      },
                  }}
                  onClick={handleHard}
              >
                  <Typography
                      variant="h1"
                      style={{
                          fontFamily: imbPlexSansKR.style.fontFamily,
                          fontWeight: 700,
                          fontSize: "48px",
                          color: "#FFFFFF",
                      }}
                  >
                      Hard
                  </Typography>
              </Button>

              <Button 
                  variant="contained"
                  sx={{ 
                      color: 'black',
                      background: '#F24C3D',
                      boxShadow: 1,
                      width:"271px",
                      height:"129px",
                      borderRadius: "25px 25px  25px 25px",
                      marginRight:"46px",
                      '&:hover': {
                          backgroundColor: '#B01103',
                      },
                  }}
                  onClick={handleSkip}
              >
                  <Typography
                      variant="h1"
                      style={{
                          fontFamily: imbPlexSansKR.style.fontFamily,
                          fontWeight: 700,
                          fontSize: "48px",
                          color: "#FFFFFF",
                      }}
                  >
                      Skip
                  </Typography>
              </Button>
            </Box>
        }

        <BorderLinearProgress variant="determinate" value={(nowNumber-1 == 0 ? 0.05 : nowNumber-1)*100/numWord} />

        </Box>

        
        
    </>
  );
};
