import Navbar from "@/components/navBar";
import { Typography } from "@mui/material";
import Box from "@mui/material/Box";
import Deck from "@/components/deck";
import { useRef, useState } from "react";
import { useEffect } from "react";
import AddDeck from "@/components/addDeck";
// import getFindAllUserdeck from "./api/getFindAlluserdeck";

import useFont from "@/hooks/fonts/useFont";

import getFindAllStarterDeck from "./api/getFindStarterDeck";
import getFindAllUserDeck from "./api/getFindAllUserDeck";
const { IBM_Plex_Sans_Thai } = useFont()
const imbPlexSansThai = IBM_Plex_Sans_Thai;

export default function Document() {
  const containerRef1 = useRef<HTMLDivElement | null>(null);
  const containerRef2 = useRef<HTMLDivElement | null>(null);
  const [userDecks, setUserDeck] = useState(<></>);
  const [starterDeck, setStarterDeck] = useState(<></>);

  // useEffect(()=>{
  //   getFindAllUserdeck().then(
  //     (res)=>{
  //       const decks = res.decks
  //       for (let index = 0; index < decks.length; index++) {
  //         if(decks[index])

  //       }
  //     }
  //   )
  // })

  useEffect(() => {
    const handleOverflow = () => {
      const element1 = containerRef1.current;
      const element2 = containerRef2.current;

      if (element1 && element2) {
        if (
          element1.scrollHeight == element1.clientHeight &&
          element1.scrollWidth == element1.clientWidth
        ) {
          element1.style.overflow = "hidden";
          element1.style.overflowX = "hidden";
          element1.style.paddingBottom = "20px";
          element2.style.paddingBottom = "20px";
        } else {
          element1.style.overflow = "hidden";
          element1.style.overflowX = "scroll";
          element1.style.paddingBottom = "30px";
          element2.style.paddingBottom = "50px";
        }
      }
    };
    handleOverflow();
    handleOverflow();
    window.addEventListener("resize", handleOverflow);
    // Cleanup the event listener when the component is unmounted
    return () => {
      window.removeEventListener("resize", handleOverflow);
    };
  }, []);

  const containerRef3 = useRef<HTMLDivElement | null>(null);
  const containerRef4 = useRef<HTMLDivElement | null>(null);


  useEffect(() => {
    getFindAllStarterDeck().then((res) => {
      if (res.decks == null) {
        console.log("stater deck empty");
      } else {
        setStarterDeck(
          res.decks.map(
            (deck: {
              experince: any;
              maxExperince: any;
              nameInLanguage: string;
              nameEng: string;
              id: any;
            }) => {
              return (
                <Deck
                  key={deck.id}
                  deckType="starter"
                  deckId={deck.id}
                  deckTitle={deck.nameInLanguage} 
                  deckMeaning={deck.nameEng} 
                  progress={Math.floor(deck.experince / deck.maxExperince)} 
                />
              );
            }
          )
        );
      }
    });

    getFindAllUserDeck().then((res) => {
      if (res.decks == null) {
        console.log("user deck empty");
      } else {
        setUserDeck(
          res.decks.map(
            (deck: {
              experince: any;
              maxExperince: any;
              nameInLanguage: string;
              nameEng: string;
              id: any;
            }) => {
              return (
                <Deck
                  key={deck.id}
                  deckType="custom"
                  deckId={deck.id}
                  deckTitle={deck.nameInLanguage} 
                  deckMeaning={deck.nameEng} 
                  progress={Math.floor(deck.experince / deck.maxExperince)} 
                />
              );
            }
          )
        );
      }
    });
  }, []);

  useEffect(() => {
    const handleOverflow = () => {
      const element1 = containerRef3.current;
      const element2 = containerRef4.current;

      if (element1 && element2) {
        if (
          element1.scrollHeight == element1.clientHeight &&
          element1.scrollWidth == element1.clientWidth
        ) {
          element1.style.overflow = "hidden";
          element1.style.overflowX = "hidden";
          element1.style.paddingBottom = "20px";
          element2.style.paddingBottom = "20px";
        } else {
          element1.style.overflow = "hidden";
          element1.style.overflowX = "scroll";
          element1.style.paddingBottom = "30px";
          element2.style.paddingBottom = "50px";
        }
      }
    };
    handleOverflow();
    handleOverflow();
    window.addEventListener("resize", handleOverflow);
    // Cleanup the event listener when the component is unmounted
    return () => {
      window.removeEventListener("resize", handleOverflow);
    };
  }, []);

  return (
    <Box sx={{ bgcolor: "#ffffff" }}>
      <Navbar />
      <Box sx={{ padding: "20px" }}>
        <Box sx={{ display: "flex", alignItems: "center" }}>
          <Typography
            variant="h1"
            style={{
              fontFamily: imbPlexSansThai.style.fontFamily,
              fontWeight: 700,
              fontSize: "48px",
              color: "#303030",
              padding: "0px 20px 0px 20px",
            }}
          >
            Normal Deck
          </Typography>
        </Box>
        <Box
          sx={{
            bgcolor: "#DDDDDD",
            margin: "20px",
            padding: "30px 40px 22px 40px",
            borderRadius: "25px",
            display: "flex",
            flexDirection: "row",
            justifyContent: "space-between",
            // gap: "10px",
          }}
        >
          {/* deck */}
          <Box
            sx={{
              display: "flex",
              flexDirection: "row",
              "&::-webkit-scrollbar": {
                width: "0.4em",
              },
              "&::-webkit-scrollbar-track": {
                boxShadow: "inset 0 0 20px #FFFFFF",
                webkitBoxShadow: "inset 0 0 20px #FFFFFF",
                borderRadius: "15px",
                // width: "2px",
              },
              "&::-webkit-scrollbar-thumb": {
                backgroundColor: "#303030",
                borderRadius: "15px",
                width: "2px",
                // outline: '1px solid slategrey'
              },
            }}
            ref={containerRef1}
          >
            {starterDeck}
          </Box>
          <Box ref={containerRef2} sx={{ paddingLeft: "40px" }}></Box>
        </Box>
      </Box>

      <Box sx={{ padding: "20px" }}>
        <Box sx={{ display: "flex", alignItems: "center" }}>
          <Typography
            variant="h1"
            style={{
              fontFamily: imbPlexSansThai.style.fontFamily,
              fontWeight: 700,
              fontSize: "48px",
              color: "#303030",
              padding: "0px 20px 0px 20px",
            }}
          >
            Custom Deck
          </Typography>
        </Box>

        <Box
          sx={{
            bgcolor: "#DDDDDD",
            margin: "20px",
            padding: "30px 40px 22px 40px",
            borderRadius: "25px",
            display: "flex",
            flexDirection: "row",
            justifyContent: "space-between",
            // gap: "10px",
          }}
        >
          {/* deck */}
          <Box
            sx={{
              display: "flex",
              flexDirection: "row",
              "&::-webkit-scrollbar": {
                width: "0.4em",
              },
              "&::-webkit-scrollbar-track": {
                boxShadow: "inset 0 0 20px #FFFFFF",
                webkitBoxShadow: "inset 0 0 20px #FFFFFF",
                borderRadius: "15px",
                // width: "2px",
              },
              "&::-webkit-scrollbar-thumb": {
                backgroundColor: "#303030",
                borderRadius: "15px",
                width: "2px",
                // outline: '1px solid slategrey'
              },
            }}
            ref={containerRef3}
          >
            {userDecks}
          </Box>

          {/* add deck btn */}
          <Box
            ref={containerRef4}
            sx={{ paddingLeft: "40px", alignItems: "center", display: "flex" }}
          >
            <AddDeck deckTitle={"Add"} deckMeaning={""} progress={0} />
          </Box>
        </Box>
      </Box>
    </Box>
  );
}
