import Navbar from "@/components/navBar";
import { Button, Typography } from "@mui/material";
import Box from "@mui/material/Box";
import TextField from "@mui/material/TextField";
import {
  JSX,
  JSXElementConstructor,
  PromiseLikeOfReactNode,
  ReactElement,
  ReactNode,
  useRef,
  useState,
} from "react";
import { useEffect } from "react";
import plusImg from "@/assets/plus.png";
import AddCard from "@/components/addCard";
import useFont from "@/hooks/fonts/useFont";

const { IBM_Plex_Sans_KR, IBM_Plex_Sans_Thai } = useFont();

import postCreateDeck from "./api/postCreateDeck";
import { useRouter } from "next/router";

export default function Document() {
  const router = useRouter()
  const [nameInLanguage, setNameInLanguage] = useState("");
  const [nameEng, setNameEng] = useState("");
  const [nameThai, setNameThai] = useState("ไม่มี");

  const [word, setWord] = useState("");
  const [pronun_1, setPronun_1] = useState("");
  const [pronun_2, setPronun_2] = useState("");
  const [pronun_3, setPronun_3] = useState("");
  const [translate, setTranslate] = useState("");
  const [id, setID] = useState(0);
  const [newDeck, setNewDeck] = useState(Array());

  const [wordBelow, setWordBelow] = useState<JSX.Element[] | JSX.Element>(
    <></>
  );

  const handleAddWord = () => {
    const newWord = {
      id,
      word,
      pronunciationLanguage: pronun_1,
      pronunciationEng: pronun_2,
      pronunciationThai: pronun_3,
      translation: translate,
    };
    console.log("add to list")
    setNewDeck((prevDeck) => [...prevDeck, newWord]);
    console.log("show below")
    setID(id + 1);
    setWord("");
    setPronun_1("");
    setPronun_2("");
    setPronun_3("");
    setTranslate("");
  };
  
  const handleDelete = (deleteID:Number) => {
    console.log(deleteID)
    let i,stop=newDeck.length;
    let tempDeck = [] 
    for(i=0;i<stop;i++){
      if(newDeck[i]?.id != deleteID){
        tempDeck.push(newDeck[i])
      }
    }
    setNewDeck(tempDeck)
  };


  useEffect(()=>{
    const showBelow = () => {
      const newWordBelow = newDeck.map(
        (card: {
          id: Number;
          word: String;
          pronunciationLanguage: String;
          pronunciationEng: String;
          pronunciationThai: String;
          translation: String;
        }) => {
      console.log("maped")
          return (
            <AddCard
              key={Number(card.id)}
              index={Number(card.id) + 1}
              word={card.word}
              pronunciationLanguage={card.pronunciationLanguage}
              pronunciationEng={card.pronunciationEng}
              pronunciationThai={card.pronunciationThai}
              translation={card.translation}
              handleDelete={handleDelete}
            />
          );
        }
      );
      setWordBelow(newWordBelow);
      console.log(wordBelow);
    };
      showBelow()
  },[newDeck])
  


  

  const handleDone = () => {
    console.log(newDeck)
    postCreateDeck(nameInLanguage,nameEng,nameThai,newDeck.map((card: {
      id: Number;
      word: String;
      pronunciationLanguage: String;
      pronunciationEng: String;
      pronunciationThai: String;
      translation: String;
    })=>{
      return({
        word : card.word,
        pronunciationLanguage : card.pronunciationLanguage,
        pronunciationEng : card.pronunciationEng,
        pronunciationThai : card.pronunciationThai,
        translation : card.translation,
      })
    }));

    router.push('/home')
  };

  return (
    <Box
      sx={{ bgcolor: "#ffffff", minWidth: "100vw", minHeight: "100vh" }}
    >
      <Navbar />
      <Box
        sx={{
          marginTop: "60px",
          marginLeft: "58px",
          width: "1797px",
          height: "288px",
          display: "flex",
          flexDirection: "row",
          justifyContent: "space-between",
        }}
      >
        <Box
          sx={{
            display: "flex",
            flexDirection: "column",
            justifyContent: "space-between",
            height: "250px",
          }}
        >
          <Box
            sx={{
              background: "#22A699",
              width: "375px",
              height: "76px",
              borderRadius: "20px 20px  20px 20px",
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
            }}
          >
            <Typography
              variant="h1"
              style={{
                fontFamily: IBM_Plex_Sans_Thai.style.fontFamily,
                fontWeight: 700,
                fontSize: "26px",
                color: "#ffffff",
              }}
            >
              Name Deck
            </Typography>
          </Box>

          <TextField
            placeholder="Enter name deck"
            variant="outlined"
            sx={{
              "& input": { marginTop: "10px" },
              "& fieldset": {
                paddingLeft: (theme) => theme.spacing(2.5),
                borderRadius: "20px",
                width: "375px",
                height: "76px",
              },
            }}
            value={nameInLanguage}
            onChange={(e) => setNameInLanguage(e.target.value)}
          />
          <TextField
            placeholder="Enter name deck"
            variant="outlined"
            sx={{
              "& input": { marginTop: "10px" },
              "& fieldset": {
                paddingLeft: (theme) => theme.spacing(2.5),
                borderRadius: "20px",
                width: "375px",
                height: "76px",
              },
            }}
            value={nameEng}
            onChange={(e) => setNameEng(e.target.value)}
          />
        </Box>

        <Box
          sx={{
            display: "flex",
            flexDirection: "column",
            justifyContent: "space-between",
            height: "250px",
          }}
        >
          <Box
            sx={{
              background: "#22A699",
              width: "375px",
              height: "76px",
              borderRadius: "20px 20px  20px 20px",
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
            }}
          >
            <Typography
              variant="h1"
              style={{
                fontFamily: IBM_Plex_Sans_Thai.style.fontFamily,
                fontWeight: 600,
                fontSize: "26px",
                color: "#ffffff",
              }}
            >
              Word
            </Typography>
          </Box>

          <Box
            sx={{
              background: "#22A699",
              width: "375px",
              height: "76px",
              borderRadius: "20px 20px  20px 20px",
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
            }}
          >
            <Typography
              variant="h1"
              style={{
                fontFamily: IBM_Plex_Sans_Thai.style.fontFamily,
                fontWeight: 600,
                fontSize: "26px",
                color: "#ffffff",
              }}
            >
              Pronunciation
            </Typography>
          </Box>

          <Box
            sx={{
              background: "#22A699",
              width: "375px",
              height: "76px",
              borderRadius: "20px 20px  20px 20px",
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
            }}
          >
            <Typography
              variant="h1"
              style={{
                fontFamily: IBM_Plex_Sans_Thai.style.fontFamily,
                fontWeight: 600,
                fontSize: "26px",
                color: "#ffffff",
              }}
            >
              Translation
            </Typography>
          </Box>
        </Box>

        <Box
          sx={{
            display: "flex",
            flexDirection: "column",
            justifyContent: "space-between",
            width: "641px",
            height: "255px",
          }}
        >
          <Box
            sx={{
              display: "flex",
              flexDirection: "row",
              justifyContent: "space-between",
              width: "641px",
              height: "86px",
            }}
          >
            <TextField
              placeholder="Enter Word"
              variant="outlined"
              value={word}
              onChange={(e) => setWord(e.target.value)}
              sx={{
                "& input": { width: "700px", fontSize: "30px",},
                "& fieldset": {
                  paddingLeft: (theme) => theme.spacing(2.5),
                  borderRadius: "20px",
                  width: "641px",
                  height: "86px",
                },
              }}
            />
          </Box>

          <Box
            sx={{
              display: "flex",
              flexDirection: "row",
              justifyContent: "space-between",
              width: "641px",
              height: "86px",
            }}
          >
            <TextField
              placeholder="Enter Pronoun"
              variant="outlined"
              value={pronun_1}
              onChange={(e) => setPronun_1(e.target.value)}
              sx={{
                "& input": { marginTop: "10px" },
                "& fieldset": {
                  paddingLeft: (theme) => theme.spacing(2.5),
                  borderRadius: "20px",
                  width: "205px",
                  height: "86px",
                },
              }}
            />

            <TextField
              placeholder="Enter Pronoun"
              variant="outlined"
              value={pronun_2}
              onChange={(e) => setPronun_2(e.target.value)}
              sx={{
                "& input": { marginTop: "10px" },
                "& fieldset": {
                  paddingLeft: (theme) => theme.spacing(2.5),
                  borderRadius: "20px",
                  width: "205px",
                  height: "86px",
                },
              }}
            />

            <TextField
              placeholder="Enter Pronoun"
              variant="outlined"
              value={pronun_3}
              onChange={(e) => setPronun_3(e.target.value)}
              sx={{
                "& input": { marginTop: "10px" },
                "& fieldset": {
                  paddingLeft: (theme) => theme.spacing(2.5),
                  borderRadius: "20px",
                  width: "205px",
                  height: "86px",
                },
              }}
            />
          </Box>
          <Box
            sx={{
              display: "flex",
              flexDirection: "row",
              justifyContent: "space-between",
              width: "641px",
              height: "86px",
            }}
          >
            <TextField
              placeholder="Enter Translation"
              variant="outlined"
              value={translate}
              onChange={(e) => setTranslate(e.target.value)}
              sx={{
                "& input": { width: "700px", fontSize: "30px", },
                "& fieldset": {
                  paddingLeft: (theme) => theme.spacing(2.5),
                  borderRadius: "20px",
                  width: "641px",
                  height: "86px",
                },
              }}
            />
          </Box>
        </Box>

        <Box
          sx={{
            height: "250px",
            width: "328px",
            border: "1px dashed grey",
            display: "flex",
            flexDirection: "column",
            justifyContent: "center",
            alignItems: "center",
          }}
          onClick={handleAddWord}
        >
          <img width="64px" height="64px" src={plusImg.src} />
          <Typography
            variant="h1"
            style={{
              fontFamily: IBM_Plex_Sans_Thai.style.fontFamily,
              fontWeight: 700,
              fontSize: "26px",
              color: "#000000",
              opacity: "50%",
            }}
          >
            Add More !
          </Typography>
        </Box>
      </Box>

      <Box
        sx={{
        overflowY:'scroll',
        height:'300px',
        width:"1820px",
        marginLeft: "40px",}}>
          {wordBelow}
      </Box>
      

      {/*done button */}
      <Box
        sx={{ display: "flex", justifyContent: "center", marginBottom: "10px" }}
      >
        <Button
          variant="contained"
          onClick={handleDone}
          sx={{
            color: "white",
            background: "#F29727",
            boxShadow: 1,
            width: "300px",
            height: "80px",
            borderRadius: "25px 25px  25px 25px",
            marginTop:"40px",
            

            "&:hover": {
              backgroundColor: "#AB5E00",
            },
          }}
        >
          <Typography
            variant="h1"
            style={{
              fontFamily: IBM_Plex_Sans_KR.style.fontFamily,
              fontWeight: 700,
              fontSize: "35px",
              color: "#FFFFFF",
             
            }}
          >
            Done
          </Typography>
        </Button>
      </Box>
      
    </Box>
  );
}
