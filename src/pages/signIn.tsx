import Link from "next/link";
import useAuth from "@/hooks/useAuth";
import { useState } from "react";
import { useRouter } from "next/router";
import postSignin from "./api/postSignin";
import getUserInfo from "./api/getUserInfo";
import Input from "@mui/joy/Input";
import { Box, Container, TextField, Button, Typography,} from "@mui/material";
import Image from "next/image";
import Image1 from "../../public/flashcards_3crad_1.svg";
import Image2 from "../../public/flashcards_3crad_2.svg";
import Image3 from "../../public/cute.png";

import useFont from "@/hooks/fonts/useFont";

const globalStyles = `
    body {
        background-color: #ffff;
    }
`;

const { M_PLUS_C1, IBM_Plex_Sans_Thai } = useFont()

const ibmPlexSansThai = IBM_Plex_Sans_Thai

// const ibmPlexSansThai5 = IBM_Plex_Sans_Thai({
//     subsets: ["thai"],
//     weight: "400",
// });

// const ibmPlexSansThai4 = IBM_Plex_Sans_Thai({
//     subsets: ["thai"],
//     weight: "400",
// });

const m_PLUS_Rounded_1c = M_PLUS_C1

export default function Document() {
    const { setAuth } = useAuth();
    const router = useRouter();

    const [username,setUsername] = useState("Jimmymonster")
    const [password,setPassword] = useState("easyPass")

    const handleSignin = () => {
        postSignin(username, password).then((res) => {
            if (res.status === 200) {
                getUserInfo().then((res) => {
                    if (res.status === 200) {
                        setAuth(res.data);
                        console.log(res.data);
                        router.push("/home");
                    }
                });
            }
        });
    };

    return (
        <>
            <style>{globalStyles}</style>
            <Box display="flex" flexDirection="column" justifyContent="center" minHeight="100vh" sx={{bgcolor:"#ffffff"}}>
                <Typography
                    variant="h1"
                    sx={{
                        fontFamily: m_PLUS_Rounded_1c.style.fontFamily,
                        fontWeight: 700,
                        color: "#22A699",
                        textAlign: "center",
                        marginBottom: "28px",
                    }}
                >
                    Synapz
                </Typography>
                <Image src={Image1} alt='image' style={{ position: "absolute", width: "40%", height: "40%", left: "0px", top: "40px"}}/>
                <Image src={Image2} alt='image' style={{ position: "absolute", width: "40%", height: "40%", right: "0px", top: "40px"}}/>
                <Typography
                    variant="h4"
                    sx={{
                        fontFamily: ibmPlexSansThai.style.fontFamily,
                        fontWeight: 700,
                        color: "#000",
                        textAlign: "center",
                        marginBottom: "70px",
                    }}
                >
                    Language Learning Flashcards
                </Typography>
                <Container maxWidth="sm">
                    <Box>
                        <Box sx={{ marginBottom: "20px" }}>
                            <Typography
                                style={{
                                    fontFamily: ibmPlexSansThai.style.fontFamily,
                                    fontWeight :400,
                                    textAlign: "left",
                                    color: "black",
                                    fontSize: "20px",
                                }}
                            >
                                User name
                            </Typography>
                            <TextField
                                fullWidth
                                sx={{}}
                                id="outlined-basic"
                                variant="outlined"
                                placeholder="Enter your user name"
                                value={username}
                                onChange={(e)=>{setUsername(e.target.value)}}
                            />
                        </Box>
                        <Box sx={{ marginBottom: "20px" }}>
                            <Typography
                                sx={{
                                    fontFamily: ibmPlexSansThai.style.fontFamily,
                                    textAlign: "left",
                                    color: "black",
                                    fontSize: "20px",
                                    fontWeight: 400,
                                }}
                            >
                                Password
                            </Typography>
                            <Input
                                type="password"
                                placeholder="Enter your password"
                                sx={{
                                    py: "16.500px",
                                    px: "14px",
                                    bgcolor: "#ffff",
                                }}
                                value={password}
                                onChange={(e)=>{setPassword(e.target.value)}}
                            />
                        </Box>
                        <Button
                            fullWidth
                            variant="contained"
                            /* <Link href="/home"> */
                            onClick={handleSignin}
                            sx={{
                                bgcolor: "#22A699",
                                py: "5px",
                                fontSize: "21px",
                                fontWeight: "700",
                                marginY: "20px",
                            }}
                        >
                            Sign In
                        </Button>
                        <Box display="flex" justifyContent="center">
                            <Typography
                                sx={{
                                    fontFamily: ibmPlexSansThai.style.fontFamily,
                                    fontWeight: 700,
                                    color: "#000",
                                    textAlign: "center",
                                }}
                            >
                                Don’t have an account ?
                            </Typography>
                            <Link href={"/signUp"}>
                                <Box>
                                    <Typography
                                        style={{
                                            fontFamily: ibmPlexSansThai.style.fontFamily,
                                            fontWeight: 800,
                                            color: "#22A699",
                                            padding: "0px 10px 0px 10px",
                                        }}
                                    >
                                        Sign Up
                                    </Typography>
                                </Box>
                            </Link>
                        </Box>
                    </Box>
                    <Box sx={{display:"flex" ,flexDirection:"row",alignItems:"center",justifyContent:"center" ,marginTop:"30px"}}>
                    <Typography
                                        style={{
                                            fontFamily: ibmPlexSansThai.style.fontFamily,
                                            fontWeight: 800,
                                            color: "black",
                                            padding: "0px 10px 0px 10px",
                                        }}
                                    >
                                        Source Code :
                        </Typography>
                        <Link href="https://gitlab.com/latenightdef/language-learning-flashcards">
                            <Typography
                                        style={{
                                            fontFamily: ibmPlexSansThai.style.fontFamily,
                                            fontWeight: 800,
                                            color: "#22A699",
                                            padding: "0px 10px 0px 10px",
                                        }}
                                    >
                                        Front-End ,
                        </Typography>
                        </Link>
                        
                        <Link href="https://gitlab.com/latenightdef/language-learning-flashcards-backend">
                        <Typography
                                        style={{
                                            fontFamily: ibmPlexSansThai.style.fontFamily,
                                            fontWeight: 800,
                                            color: "#22A699",
                                            padding: "0px 10px 0px 10px",
                                        }}
                                    >
                                        Back-End
                        </Typography>
                        </Link>
                        
                    </Box>
                </Container>
                <Image src={Image3} alt='image' style={{ position: "absolute", width: "25rem",  height: "24rem", bottom: "0"}}/> 
            </Box>
        </>
    );
}