import TotalXp from "@/components/TotalXp";

import Navbar from "@/components/navBar"
import { Box,Container,Typography } from "@mui/material"
import profilePicture from "@/assets/profile1.png";
import Order1Picture from "@/assets/Order/1.png"
import Order2Picture from "@/assets/Order/2.png"
import Order3Picture from "@/assets/Order/3.png"
import Order4Picture from "@/assets/Order/4.png"
import Order5Picture from "@/assets/Order/5.png"
// import { IBM_Plex_Sans_Thai } from "next/font/google";
import profile1 from "../assets/Profiles/profile_1.png";
import profile2 from "../assets/Profiles/profile_2.png";
import profile3 from "../assets/Profiles/profile_3.png";
import profile4 from "../assets/Profiles/profile_4.png";
import profile5 from "../assets/Profiles/profile_5.png";
import profile6 from "../assets/Profiles/profile_6.png";
import profile7 from "../assets/Profiles/profile_7.png";

import getLeaderBoard from "./api/getLeaderBoard";
import { useEffect, useState } from "react";

import useFont from "@/hooks/fonts/useFont";

const { IBM_Plex_Sans_Thai } = useFont()
const imbPlexSansThai = IBM_Plex_Sans_Thai

let lengthUsers = 0

export default function Document() {

    const [users,setUsers] = useState([{username:"",totalExp:0},{username:"",totalExp:0},{username:"",totalExp:0},{username:"",totalExp:0},{username:"",totalExp:0}])

    useEffect(()=>{
        getLeaderBoard(5).then((res)=>{
            lengthUsers = res.ListUsers.length
            setUsers(res.ListUsers)
            console.log(Array(res.ListUsers))
        })
    },[])

    const Profile =(user:any)=>{
        if(user?.profileImage=="profile1"){
            return profile1
        }else if(user?.profileImage=="profile2"){
            return profile2
        }else if(user?.profileImage=="profile3"){
            return profile3
        }else if(user?.profileImage=="profile4"){
            return profile4
        }else if(user?.profileImage=="profile5"){
            return profile5
        }else if(user?.profileImage=="profile6"){
            return profile6
        }else{
            return profile7
        }
    }

    return (
        <Box sx={{
            bgcolor:"#ffffff",
            minHeight:"100vh"
        }}>
            <Navbar/>
            <Container sx={{
                display:"flex",
                flexDirection:'column'
            }}>
                {lengthUsers >= 1 ? (
                    <Box position="relative" sx={{
                        bgcolor:"#007469",
                        height:"160px",
                        width:"1200px",
                        marginTop:"22px",
                        borderRadius:"25px 25px 25px 25px",
                        display: "flex", 
                        justifyContent: "space-around" ,
                        alignItems: "center"
                    }}>
                        <img
                            width="100px"
                            height="100px"
                            src={Order1Picture.src}
                            alt="orderImage"
                        />
                        <img
                            width="100px"
                            height="100px"
                            src={Profile(users[0]).src}
                            alt="profileImage"
                        />
                        <Typography
                            variant="h1"
                            style={{
                                fontFamily: imbPlexSansThai.style.fontFamily,
                                fontWeight: 700,
                                fontSize: "46px",
                                color: "#ffffff",
                                width:"400px"
                            }}
                        >
                            {users[0].username}
                        </Typography>

                        <TotalXp totalXp={users[0].totalExp} />
                    </Box>
                ) : (<div></div>)}
                {lengthUsers >= 2 ? (
                    <Box position="relative" sx={{
                        bgcolor:"#007469",
                        height:"160px",
                        width:"1200px",
                        marginTop:"22px",
                        borderRadius:"25px 25px 25px 25px",
                        display: "flex", 
                        justifyContent: "space-around" ,
                        alignItems: "center"
                    }}>
                        <img
                            width="100px"
                            height="100px"
                            src={Order2Picture.src}
                            alt="orderImage"
                        />
                        <img
                            width="100px"
                            height="100px"
                            src={Profile(users[1]).src}
                            alt="profileImage"
                        />
                        <Typography
                            variant="h1"
                            style={{
                                fontFamily: imbPlexSansThai.style.fontFamily,
                                fontWeight: 700,
                                fontSize: "46px",
                                color: "#ffffff",
                                width:"400px"
                            }}
                        >
                            {users[1].username}
                        </Typography>

                        <TotalXp totalXp={users[1].totalExp} />
                    </Box>
                ) : (<div></div>)}
                {lengthUsers >= 3 ? (
                    <Box position="relative" sx={{
                        bgcolor:"#007469",
                        height:"160px",
                        width:"1200px",
                        marginTop:"22px",
                        borderRadius:"25px 25px 25px 25px",
                        display: "flex", 
                        justifyContent: "space-around" ,
                        alignItems: "center"
                    }}>
                        <img
                            width="100px"
                            height="100px"
                            src={Order3Picture.src}
                            alt="orderImage"
                        />
                        <img
                            width="100px"
                            height="100px"
                            src={Profile(users[2]).src}
                            alt="profileImage"
                        />
                        <Typography
                            variant="h1"
                            style={{
                                fontFamily: imbPlexSansThai.style.fontFamily,
                                fontWeight: 700,
                                fontSize: "46px",
                                color: "#ffffff",
                                width:"400px"
                            }}
                        >
                            {users[2].username}
                        </Typography>

                        <TotalXp totalXp={users[2].totalExp} />
                    </Box>
                ) : (<div></div>)}
                {lengthUsers >= 4 ? (
                    <Box position="relative" sx={{
                        bgcolor:"#007469",
                        height:"160px",
                        width:"1200px",
                        marginTop:"22px",
                        borderRadius:"25px 25px 25px 25px",
                        display: "flex", 
                        justifyContent: "space-around" ,
                        alignItems: "center"
                    }}>
                        <img
                            width="100px"
                            height="100px"
                            src={Order4Picture.src}
                            alt="orderImage"
                        />
                        <img
                            width="100px"
                            height="100px"
                            src={Profile(users[3]).src}
                            alt="profileImage"
                        />
                        <Typography
                            variant="h1"
                            style={{
                                fontFamily: imbPlexSansThai.style.fontFamily,
                                fontWeight: 700,
                                fontSize: "46px",
                                color: "#ffffff",
                                width:"400px"
                            }}
                        >
                            {users[3].username}
                        </Typography>

                        <TotalXp totalXp={users[3].totalExp} />
                    </Box>
                ) : (<div></div>)}
                {lengthUsers >= 5 ? (
                    <Box position="relative" sx={{
                        bgcolor:"#007469",
                        height:"160px",
                        width:"1200px",
                        marginTop:"22px",
                        borderRadius:"25px 25px 25px 25px",
                        display: "flex", 
                        justifyContent: "space-around" ,
                        alignItems: "center"
                    }}>
                        <img
                            width="100px"
                            height="100px"
                            src={Order5Picture.src}
                            alt="orderImage"
                        />
                        <img
                            width="100px"
                            height="100px"
                            src={Profile(users[4]).src}
                            alt="profileImage"
                        />
                        <Typography
                            variant="h1"
                            style={{
                                fontFamily: imbPlexSansThai.style.fontFamily,
                                fontWeight: 700,
                                fontSize: "46px",
                                color: "#ffffff",
                                width:"400px"
                            }}
                        >
                            {users[4].username}
                        </Typography>

                        <TotalXp totalXp={users[4].totalExp} />
                    </Box>
                ) : (<div></div>)}  
            </Container>
        </Box>
    )
  }