import React, { useState } from "react";
import { Component } from "react";
import {
  Badge,
  Box,
  Button,
  Container,
  GlobalStyles,
  IconButton,
  InputAdornment,
  Link,
  TextField,
  Typography,
  containerClasses,
} from "@mui/material";
import { createTheme } from "@mui/material/styles";
import owl from "./assets/owl.png";

import { Visibility, VisibilityOff } from "@mui/icons-material";
import useAuth from "@/hooks/useAuth";
import { useRouter } from "next/router";

import postSignup from "./api/postSignup";
import getUserInfo from "./api/getUserInfo";
import useFont from "@/hooks/fonts/useFont";

const { IBM_Plex_Sans_Thai } = useFont()

const ibmPlexSansThai = IBM_Plex_Sans_Thai

const App = () => {
  const { setAuth } = useAuth();
  const router = useRouter();

  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [nickname, setNickname] = useState("");

  const handleSignup = () => {
    console.log(username, password, nickname);
    postSignup(username, password, nickname).then((res) => {
      if (res.status === 201) {
        getUserInfo().then((res) => {
          if (res.status === 200) {
            setAuth(res.data);
            console.log(res.data);
            router.push("/home");
          }
        });
      } else {
        console.log(String(res.status));
      }
    });
  };

  return (
    <Box
      sx={{
        bgcolor: "#22A699",
        p: "0px",
        position: "relative",
      }}
    >
      <img
        src="https://cdn.discordapp.com/attachments/1158294285343543336/1158837190932037704/owl.png?ex=651db2bc&is=651c613c&hm=ed03f36b371c9ef24ca022b01d82026d70f82d3a629edffd6fda14e05ca9eb26&"
        alt="owl"
        style={{
          position: "absolute",
          height: "500px",
          left: "180px",
          top: "170px",
        }}
      />
      <Box sx={{ textAlign: "center" }}>
        <Box
          sx={{
            px: "180px",
            bgcolor: "#FFFFFF",
            marginLeft: "36%",
            height: "100vh",
            borderRadius: "20px 0px 0px 20px",
          }}
        >
          <Box>
            <Typography
              style={{
                textAlign: "center",
                color: "black",
                paddingTop: "30px",
                paddingBottom: "20px",
                fontSize: "56px",
                fontWeight: "600",
              }}
            >
              Create New Account
            </Typography>
          </Box>

          <Box>
            <Typography
              style={{
                textAlign: "center",
                color: "black",
                marginBottom: "50px",
                fontSize: "24px",
                fontWeight: "400",
              }}
            >
              Already have an account?{" "}
              <Link href="/" color="inherit">
                Log in
              </Link>
            </Typography>
          </Box>

          <Box sx={{ marginBottom: "20px" }}>
            <Typography
              style={{
                textAlign: "left",
                color: "black",
                fontSize: "26px",
                fontWeight: "400",
              }}
            >
              User name
            </Typography>
            <TextField
              key={1}
              fullWidth
              sx={{}}
              id="outlined-basic"
              variant="outlined"
              placeholder="Enter your user name"
              value={username}
              onChange={(e) => {
                setUsername(e.target.value);
              }}
            />
          </Box>

          <Box sx={{ marginBottom: "20px" }}>
            <Typography
              style={{
                textAlign: "left",
                color: "black",
                fontSize: "26px",
                fontWeight: "400",
              }}
            >
              Nick name
            </Typography>
            <TextField
                key={2}
              fullWidth
              sx={{ bgcolor: "white" }}
              id="outlined-basic"
              variant="outlined"
              placeholder="Enter your nick name"
              value={nickname}
              onChange={(e) => {
                setNickname(e.target.value);
              }}
            />
          </Box>

          <Box sx={{ marginBottom: "40px" }}>
            <Typography
              style={{
                textAlign: "left",
                color: "black",
                fontSize: "26px",
                fontWeight: "400",
              }}
            >
              Create a password
            </Typography>
            <TextField
            key={3}
              fullWidth
              type="password"
              id="outlined-basic"
              variant="outlined"
              placeholder="Enter your password"
              value={password}
              onChange={(e) => {
                setPassword(e.target.value);
              }}
            />
          </Box>

          <Button
            fullWidth
            variant="contained"
            sx={{
              bgcolor: "#22A699",
              py: "15px",
              fontSize: "26px",
              fontWeight: "700",
            }}
            onClick={handleSignup}
          >
            Create an account
          </Button>
        </Box>
      </Box>
    </Box>
  );
};

export default function signUp() {
  return App();
}
