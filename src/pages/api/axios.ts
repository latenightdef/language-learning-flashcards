import axios from 'axios'

export default axios.create({
    baseURL: "https://synapz-backend.techtransthai.org"
})