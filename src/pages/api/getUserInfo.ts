import axios
 from "./axios";
const getUserInfo = async () => {
    const res = await axios.get(
        '/users/me',
        {
            headers: {
            Authorization : `Bearer ${localStorage.getItem("accessToken")}`
            }
        }
    );
    console.log(res.data)
    return {
        status : res.status,
        data : {
            userId : res.data.id,
            loginStreak: Number(res.data.loginSteak),
            totalXp: Number(res.data.totalExp),
            username: res.data.username,
            nickname : res.data.nickname,
            profileImage : res.data.profileImage,
            rank : res.data.ranking
        }
    };
};

export default getUserInfo