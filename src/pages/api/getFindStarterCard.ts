import axios
 from "./axios";
const getFindCard = async (deckID:Number) => {
    const res = await axios.get(
        `/deck/starter/${deckID}`,
        {
            headers: {
            Authorization : `Bearer ${localStorage.getItem("accessToken")}`
            }
        }
    );
    return {
        status: res.status,
        cards: res.data
    };
};

export default getFindCard