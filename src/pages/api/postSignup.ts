import axios
 from "./axios";
const postSignin = async (username:string, password:string, nickname:string) => {
    const res = await axios.post(
        '/auth/signup',
        {
            username : username,
            password : password,
            nickname : nickname
        }
    );
    if (res.data.access_token){
        localStorage.setItem("accessToken",res.data.access_token)
    }
    return {
        status: res.status
    };
};

export default postSignin