import axios
 from "./axios";
const getLeaderBoard = async (num:Number) => {
    const res = await axios.get(
        `/users/leaderboard/${num}`,
        {
            headers: {
            Authorization : `Bearer ${localStorage.getItem("accessToken")}`
            }
        }
    );
    return {
        status: res.status,
        ListUsers: res.data
    };
};

export default getLeaderBoard