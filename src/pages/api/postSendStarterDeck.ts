import axios
 from "./axios";
const postSendStarterDeck = async (deckID:Number, data:object) => {
    console.log(data)
    const res = await axios.post(
        `/play/starter/deck/${deckID}`,
        {
            cards: data
        },
        {
            headers: {
                Authorization : `Bearer ${localStorage.getItem("accessToken")}`,
                },
        }
        
    );
    if(res.data=="ok"){
        console.log(res.data)
    }
    return {
        status: res.status,
        data: res.data
    };
};

export default postSendStarterDeck