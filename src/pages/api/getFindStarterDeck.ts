import axios
 from "./axios";
const getFindAllStarterDeck = async () => {
    const res = await axios.get(
        '/deck/starter/expData',
        {
            headers: {
            Authorization : `Bearer ${localStorage.getItem("accessToken")}`
            }
        }
    );
    return {
        status: res.status,
        decks: res.data
    };
};

export default getFindAllStarterDeck