import axios
 from "./axios";
const postUserProfileEdit = async (profileImage:string) => {
    const res = await axios.patch(
        '/users/edit',
        {
            profileImage : profileImage
        },
        {
            headers: {
                Authorization : `Bearer ${localStorage.getItem("accessToken")}`,
                },
        }
    );
    return {
        status: res.status
    };
};

export default postUserProfileEdit