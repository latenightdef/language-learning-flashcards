import axios
 from "./axios";
const postSignin = async (username:string, password:string) => {
    const res = await axios.post(
        '/auth/signin',
        {
            username : username,
            password : password
        }
    );
    if (res.data.access_token){
        localStorage.setItem("accessToken",res.data.access_token)
    }
    return {
        status: res.status
    };
};

export default postSignin