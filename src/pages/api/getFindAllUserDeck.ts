import axios
 from "./axios";
const getFindAllUserDeck = async () => {
    const res = await axios.get(
        '/deck',
        {
            headers: {
            Authorization : `Bearer ${localStorage.getItem("accessToken")}`
            }
        }
    );
    return {
        status: res.status,
        decks: res.data
    };
};

export default getFindAllUserDeck