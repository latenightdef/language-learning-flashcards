import axios
 from "./axios";
const postCreateDeck = async (name1:String,name2:String,name3:String, cards:object) => {
    console.log(cards)
    const res = await axios.post(
        '/deck/create',
        {
            nameInLanguage: name1,
            nameEng: name2,
            nameThai: name3,
            cards: cards
        },
        {
            headers: {
                Authorization : `Bearer ${localStorage.getItem("accessToken")}`,
                },
        }
    );
    if(res.data=="ok"){
        console.log(res.data)
    }
    return {
        status: res.status,
        data: res.data
    };
};

export default postCreateDeck