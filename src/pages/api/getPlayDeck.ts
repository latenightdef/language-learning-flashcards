import axios
 from "./axios";
const getPlayDeck = async (deckID:Number) => {
    const res = await axios.get(
        `/play/deck/${deckID}`,
        {
            headers: {
            Authorization : `Bearer ${localStorage.getItem("accessToken")}`
            }
        }
    );
    return {
        status: res.status,
        cards: res.data
    };
};

export default getPlayDeck