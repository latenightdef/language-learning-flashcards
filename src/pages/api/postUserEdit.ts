import axios
 from "./axios";
const postUserEdit = async (username:string, password:string, newPassword:string, nickname:string) => {
    const res = await axios.patch(
        '/users/edit',
        {
            username : username,
            password : password,
            newPassword : newPassword,
            nickname : nickname
        },
        {
            headers: {
                Authorization : `Bearer ${localStorage.getItem("accessToken")}`,
                },
        }
    );
    return {
        status: res.status
    };
};

export default postUserEdit