import axios
 from "./axios";
const postSendDeck = async (deckID:Number, data:object) => {
    console.log(data)
    const res = await axios.post(
        `/play/deck/${deckID}`,
        {
            cards: data
        },
        {
            headers: {
                Authorization : `Bearer ${localStorage.getItem("accessToken")}`,
                },
        }
    );
    if(res.data=="ok"){
        console.log(res.data)
    }
    return {
        status: res.status,
        data: res.data
    };
};

export default postSendDeck