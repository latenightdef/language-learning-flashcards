FROM alpine

# Set up environment for building
RUN apk add yarn nodejs

# Copy files to build environment
RUN mkdir /opt/llf
COPY . /opt/llf

WORKDIR /opt/llf
RUN yarn
RUN yarn run build

CMD yarn run start
