![Synapz Logo](src/assets/owl.png)
### Synapz - Language Learning Flashcards

เว็บไซต์สำหรับสร้างและเล่นบัตรคำศัพท์เพื่อการเรียนภาษาต่างประเทศ เขียนด้วย [NextJS](https://nextjs.org/) และ [Material UI](https://mui.com/)

โปรเจกต์นี้เป็นส่วนหนึ่งของวิชา THEORY OF COMPUTATION

# การรันโปรเจกต์

1. โคลนโปรเจกต์

```
$ https://gitlab.com/latenightdef/language-learning-flashcards.git
$ cd language-learning-flashcards
```

2. รันโปรเจกต์

```
$ yarn
$ yarn run build
$ yarn run start
```


